{ pkgs ? import <nixpkgs> {} }:
{
packages = [
   (pkgs.nixopsUnstable.override {
     ## https://github.com/NixOS/nixops/issues/1490
     overrides = (self: super: {
       nixops = super.nixops.overridePythonAttrs (
         _: {
           src = pkgs.fetchgit {
             url = "https://github.com/nixos/nixops";
             rev = "35ac02085169bc2372834d6be6cf4c1bdf820d09";
             sha256 = "1jh0jrxyywjqhac2dvpj7r7isjv68ynbg7g6f6rj55raxcqc7r3j";
           };
         }
       );
     });
   })
 ];

}
