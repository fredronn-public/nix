{
    imports = [ ./ssh_ca.nix ];
    config = {
        services.openssh.enable = true;
    };
}
