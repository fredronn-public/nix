{pkgs, lib, config, ...}:{
  config = {
    programs.mosh.enable = true;
    ronnvall.nftables.allowedUDPPorts = [ "60000-61000" ];
  };
}
