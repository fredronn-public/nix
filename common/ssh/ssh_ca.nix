{ config, ... }:{
    config = {
        services.openssh.extraConfig = ''
        TrustedUserCAKeys               /etc/ssh/ca.pub
        '';
        sops.secrets."ssh/ca" = {};
        environment.etc."ssh/ca.pub".source = config.sops.secrets."ssh/ca".path;
    };
}
