# Sets fish as default shell
{ lib, pkgs, ...}:{
    config = {
        users.defaultUserShell = pkgs.fish;
        programs.fish = {
            enable = true;
            shellInit = ''
            set fish_greeting
            '';
        };
        environment.systemPackages = [ pkgs.neovim ];
        environment.variables = {
            EDITOR = "nvim";
            VISUAL = "nvim";
        };
    };
}
