{ lib, pkgs, config, home-manager, ... }:
with lib;
let
  cfg = config.ronnvall.users;
in {
    options.ronnvall.users = {
        enable = mkOption { default = true; };
        userDefaults = mkOption {
            default = {
                isNormalUser=true;
                extraGroups=["wheel" "docker" "libvirtd" "video"];
                description="Fredrik Rönnvall";
                passwordFile=config.sops.secrets."passwords/user".path;
            };
        };

        rootUser = mkOption {
            default = {
                passwordFile = config.sops.secrets."passwords/root".path;
            };
        };

        managed = mkOption {
            default = [ "fredrik" ];
        };
    };

    imports = [ ./fish.nix ];
    
    config = mkIf cfg.enable {
        sops.secrets."passwords/user".neededForUsers = true;
        sops.secrets."passwords/root".neededForUsers = true;
        users.users = listToAttrs (map (x: {name="${x}"; value=cfg.userDefaults;}) cfg.managed);
    };
}
