{ lib, pkgs, config, sops-nix, options, ... }: with lib; {
    imports = [
        ./system
        ./backups
        ./monitoring
        ./networking
        ./kubernetes
        ./servers
        ./ssh
        ./users
        ./software
        ./chalmers.nix
        ./hardware
    ];
    options = {
        ronnvall.system.type = mkOption { default = "server"; };
    };
    config = {
        system.stateVersion = "23.05";
        boot.kernelPackages = mkDefault pkgs.linuxPackages_6_1;
        time.timeZone = mkDefault "UTC";
    };
}
