{
    config = {
        services.locate.enable = true;
        services.locate.locate = pkgs.plocate;
        services.locate.localuser = null;
        services.locate.output = "/var/lib/plocate/plocate.db";
        users.groups.plocate.name = "plocate";
    };
}
