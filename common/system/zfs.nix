{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.zfs;
  docker = config.virtualisation.docker.enable;
  podman = config.virtualisation.podman.enable;
  zedNotify = pkgs.writeScript "zed-notify-apprise.sh" ''
    #!${pkgs.bash}/bin/bash
    APPRISE="${pkgs.apprise}/bin/apprise"
    ${pkgs.coreutils}/bin/cat - | $APPRISE -c /etc/apprise.conf
  '';

in {
    options.ronnvall.zfs = {
        enable = mkOption { default = false; };
        persist = mkOption { default = true; };
        zroot = mkOption { default = "zroot"; };
        rollback = mkOption { default = true; };
    };

    config = mkIf cfg.enable (mkMerge [ {
        boot.zfs.enableUnstable = false;
        boot.zfs.devNodes = "/dev/disk/by-id";
        boot.supportedFilesystems = [ "zfs" ];
        services.zfs.zed.settings = {
            ZED_EMAIL_ADDR = [ "root@localhost" ];
            ZED_EMAIL_PROG = "${zedNotify}";
            ZED_NOTIFY_INTERVAL_SECS = 3600;
            ZED_NOTIFY_VERBOSE = false;
        };
        boot.kernelParams = lib.mkDefault [ "nohibernate" ];
        services.openssh.hostKeys = mkIf cfg.persist [
          {
            bits = 4096;
            path = "/persist/ssh_host_rsa_key";
            type = "rsa";
          }
          {
            path = "/persist/ssh_host_ed25519_key";
            type = "ed25519";
          }
        ];
        services.zfs = {
            trim.enable = true;
            autoScrub.enable = true;
            autoScrub.interval = "monthly";
            autoSnapshot.enable = true;
            autoSnapshot.frequent = 0;
            autoSnapshot.hourly = 0;
        };

        systemd.timers.zfs-snapshot-frequent.enable = false;
        systemd.timers.zfs-snapshot-hourly.enable = false;

        environment.systemPackages = with pkgs; [
            sanoid
            mbuffer
            lzop
        ];
    }
    (mkIf cfg.rollback  {
        boot.initrd.postDeviceCommands = lib.mkAfter ''
        zfs list -t snap -r ${cfg.zroot} -H -o name | grep '@rollback$' | xargs -n1 zfs rollback -r 
        '';
    })
    (mkIf docker {
        virtualisation.docker.storageDriver = "overlay2";
        fileSystems."/var/lib/docker" = { device = "/dev/${cfg.zroot}/var/lib/docker"; fsType = "xfs"; };
    })
    (mkIf cfg.persist {
        sops.age.sshKeyPaths = [
            "/persist/ssh_host_ed25519_key"
        ];
        ronnvall.restic.backupPaths = [ "/persist" ];
    })
    ]);
}
