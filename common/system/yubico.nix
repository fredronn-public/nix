{
    config = {
        security.pam.yubico = {
            enable = true;
            debug = false;
            mode = "challenge-response";
        };
    };
}
