{ pkgs, config, lib, ... }:with lib;
let
    cfg = config.ronnvall.journald;
in
{
    imports = [ ./zfs.nix ./nixos-upgrade.nix ./yubico.nix ./btrfs.nix ];
    options = {
        ronnvall.journald = {
            storage = mkOption { default = "volatile"; };
            size = mkOption { default = "16M"; };
        };
    };
    config = {
        boot.extraModprobeConfig = ''
        options hid_apple fnmode=0
        '';
        services.cron.enable = true;
        services.fstrim.enable = true;
        services.logind.extraConfig = ''
            HandlePowerKey=suspend
        '';
        nix.settings.experimental-features = [ "nix-command" "flakes" ];
        documentation.nixos.enable = false;
        documentation.man.enable = false;
        hardware.enableRedistributableFirmware = true;
        nix.settings.auto-optimise-store = true;
        nix.gc.automatic = true;
        nix.gc.options = "--delete-older-than 7d";
        nix.gc.dates = "daily";
        i18n.defaultLocale = "en_GB.UTF-8";
        i18n.supportedLocales = [
            "en_GB.UTF-8/UTF-8"
            "en_US.UTF-8/UTF-8"
            "sv_SE.UTF-8/UTF-8"
        ];
        services.journald.extraConfig = ''
        SystemMaxUse=${cfg.size}
        Storage=${cfg.storage}
        '';
    };
}
