{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.btrfs;
in {
    options.ronnvall.btrfs = {
        enable = mkOption { default = false; };
    };

    config = mkIf cfg.enable {
        services.btrbk.instances.home.settings = {
            snapshot_preserve = "14d";
            snapshot_preserve_min = "2d";
            snapshot_dir = "/home/.snapshots";
            subvolume = "/home";
        };
    };
}
