{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.autoUpdate;
  systemType = config.ronnvall.system.type;
in {
    options.ronnvall.autoUpdate = {
        enable = mkOption {
            default = true;
        };
        hostName = mkOption {
            type = types.str;
            default = config.networking.hostName;
        };
    };

    config = mkIf cfg.enable {
        system.autoUpgrade = {
            enable = true;
            allowReboot = (systemType == "server");
            #allowReboot = false;
            dates = "daily";
            flags = [ "--impure" ];
            flake = "${./../..}";
        };
    };
}

