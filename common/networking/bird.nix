{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.bird;
in {
    options.ronnvall.bird = {
        enable = mkEnableOption "bird routing";
        routerId = mkOption {
            type = types.str;
        };
        ospfInterfaces = mkOption {
            default = [ ];
        };
    };

    config = mkIf cfg.enable {
        ronnvall.nftables.noNatPorts = [ "bird1" ]; # Never NAT this SRC
        systemd.network.enable = true;
        systemd.network.netdevs."bird1".netdevConfig = {
            Kind = "dummy";
            Name = "bird1";
        };
        systemd.network.networks."bird1".address = [ "${cfg.routerId}/32" ];
        systemd.network.networks."bird1".name = "bird1";
        sops.secrets."bird/ospf/password".sopsFile = ./bird.yaml;
        services.bird2 = {
            enable = true;
            config = ''
router id ${cfg.routerId};

ipv4 table master4;
ipv6 table master6;

filter redist_filter {
    if (net ~ [172.16.0.0/12{16,32}]) then accept;
    if (net ~ [192.168.0.0/16{24,32}]) then accept;
    if (net ~ [10.200.101.0/24{24,32}]) then accept;
    reject;
}

filter host_route_filter {
    if (net ~ [10.200.101.0/24{32,32}]) then accept;
    reject;
}

protocol device {
    scan time 10;
    interface "br-container";
    interface "bird*";
}

protocol kernel {
    learn;
    scan time 20;

    # Use explicit kernel route metric to avoid collisions
    metric 1024;

    ipv4 {
        import filter host_route_filter;
        # Only export routes received over OSPF
        export filter redist_filter;
    };
}

protocol direct {
    ipv4 {
        import filter redist_filter;
    };
    interface "br-*";
    interface "e*";
}

protocol ospf v2 {
    ipv4 {
        # Import all from ospf
        import filter redist_filter;
        # Only export static routes defined above
        export filter {
            if source = RTS_DEVICE then accept;
            if source = RTS_INHERIT then accept;
            reject;
        };
    };
    area 0 {
        networks {
            172.16.0.0/12; 10.200.101.0/24;
        };

        interface "bird1" {
            stub yes;
        };
        
        ${optionalString (cfg.ospfInterfaces != [ ]) ''
        ${concatMapStrings (x: ''
        interface "${x}" {
            authentication cryptographic;
            password "7b6e4191bfc96ea3be84cac05afe4714" {
                id 5;
            };
        };
        '') cfg.ospfInterfaces}
        ''}
   };
}
'';
        };
    };
}
