{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.containerbridge;
in {
    options.services.containerbridge = {
        enable = mkEnableOption "containerbridge";
        networkAddress = mkOption {
            default = [ ];
        };
        name = mkOption {
            default = "br-container";
        };
    };

    config = mkIf cfg.enable {
        systemd.network.netdevs."${cfg.name}".netdevConfig = {
            Kind = "bridge";
            Name = cfg.name;
        };
        systemd.network.networks."${cfg.name}" = {
            name = cfg.name;
            networkConfig = {
                Address = cfg.networkAddress;
                IPForward = "yes";
                DHCPServer = "yes";
                ConfigureWithoutCarrier = "yes";
            };
        };
    };
}
