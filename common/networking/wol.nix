{ pkgs, lib, config, ... }:
with lib;
let
    cfg = config.ronnvall.wol;
    hostName = config.networking.hostName;
in
{
    options.ronnvall.wol = {
        enable = mkOption { default = true; };
        topic = mkOption { default = "wake-on-lan/${hostName}"; };
        mqtt_server = mkOption { default = "172.16.1.1"; };
        action_trigger = mkOption { default = "off"; };
        action_commands = mkOption { default = [ "${pkgs.systemd}/bin/systemctl poweroff" ]; };
        interface = mkOption { default = "eno1"; };
    };
    config = mkIf cfg.enable {
        systemd.services.mqtt_wol_monitor = {
            enable = true;
            serviceConfig.Type = "simple";
            serviceConfig.Restart = "always";
            serviceConfig.RestartSec = 60;
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" ];
            restartIfChanged = true;
            script = ''
                HOST="${cfg.mqtt_server}"
                TOPIC="${cfg.topic}"
                COMMAND="${pkgs.mosquitto}/bin/mosquitto_sub"
                COMMAND="$COMMAND -h $HOST -t $TOPIC"
                ENABLE_WOL="${pkgs.ethtool}/bin/ethtool -s ${cfg.interface} wol g"
                $COMMAND | while read word; do
                    echo received $word
                    if [ "$word" = "${cfg.action_trigger}" ]; then
                        echo enable wake on lan...
                        $ENABLE_WOL
                        ${concatMapStrings (x: ''
                            echo running ${x}
                            ${x}
                        '') cfg.action_commands}
                    fi
                done
            '';
        };
    };
}
