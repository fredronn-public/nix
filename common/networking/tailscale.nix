{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.ronnvall.tailscale;
in
{
  options.ronnvall.tailscale = {
    enable = mkOption { default = false; };
  };
  config = mkIf cfg.enable {
    ronnvall.bird.ospfInterfaces = [ "tailscale0" ];
    sops.secrets."tailscale-key".sopsFile = ./tailscale.yaml;
    services.tailscale.enable = true;
    environment.systemPackages = with pkgs; [ tailscale ];
    systemd.services.tailscale-autoconnect = {
      description = "Automatic connection to Tailscale";
      after = [ "network-pre.target" "tailscale.service" ];
      wants = [ "network-pre.target" "tailscale.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig.Type = "oneshot";
      script = with pkgs; ''
        # wait for tailscaled to settle
        sleep 2

        # check if we are already authenticated to tailscale
        status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
        if [ $status = "Running" ]; then # if so, then do nothing
          exit 0
        fi

        TAILSCALE_KEY=$(cat ${config.sops.secrets."tailscale-key".path})

        # otherwise authenticate with tailscale
        ${tailscale}/bin/tailscale up -authkey $TAILSCALE_KEY
      '';
    };
  };
}