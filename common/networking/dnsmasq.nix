{ pkgs, config, lib, ... }:
with lib;
let
    cfg = config.ronnvall.dhcp;
in
{
    options.ronnvall.dhcp = {
        enable = mkOption { default = false; };
        interfaces = mkOption { default = [ "lan" "isolated" "iot" "mgmt" ]; };
    };
    config = mkIf cfg.enable {
        systemd.services."kea-dhcp4-server".serviceConfig = {
            RestartSec = 60;
            Restart = "always";
        };
        services.resolved.enable = false;
        services.unbound = {
            enable = true;
            settings = {
                server = {
                    interface = [ "0.0.0.0" ];
                    access-control = [
                        "127.0.0.0/8 allow"
                        "10.0.0.0/8 allow"
                        "172.16.0.0/12 allow"
                        "192.168.0.0/16 allow"
                    ];
                    num-threads = 2;
                    rrset-cache-size = "128m";
                    msg-cache-size = "64m";
                    so-rcvbuf = "1m";
                    msg-cache-slabs = 8;
                    rrset-cache-slabs = 8;
                    infra-cache-slabs = 8;
                    key-cache-slabs = 8;
                    cache-min-ttl = 1800;
                    local-zone = [
                        "\"ronnvall.com\" transparent"
                    ];
                    local-data = [
                        "\"ldap.ronnvall.com 10 A 10.200.101.68\""
                    ];
                };
                forward-zone = [{
                    name = ".";
                    forward-addr = [ "1.1.1.1" "1.0.0.1" "8.8.8.8" "8.8.4.4" "9.9.9.9" ];
                }];

            };
        };

        services.kea = {
            ctrl-agent = {
                enable = true;
                settings = {
                    http-host = "0.0.0.0";
                    http-port = 18888;
                    control-sockets = {
                        dhcp4 = {
                            socket-type = "unix";
                            socket-name = "/run/kea/dhcp4.socket";
                        };
                    };
                };
            };
            dhcp4 = {
                enable = true;
                settings = {
                    valid-lifetime = 86400;
                    interfaces-config = { interfaces = cfg.interfaces; };
                    control-socket = {
                        socket-type = "unix";
                        socket-name = "/run/kea/dhcp4.socket";
                    };
                    hooks-libraries = [
                        { library = "${pkgs.kea}/lib/kea/hooks/libdhcp_lease_cmds.so"; parameters = {}; }
                    ];
                    subnet4 = [{
                        subnet = "172.16.1.0/24";
                        pools = [{ pool = "172.16.1.100 - 172.16.1.240"; }];
                        option-data = [
                            { name = "routers"; data = "172.16.1.1"; }
                            { name = "domain-name-servers"; data = "172.16.1.1"; }
                        ];
                        reservations = [
                            { hw-address = "00:31:92:a8:87:78"; ip-address = "172.16.1.10"; hostname = "tplinkap1"; }
                            { hw-address = "1c:69:7a:0b:4b:85"; ip-address = "172.16.1.11"; hostname = "nuc"; }
                            { hw-address = "e4:11:5b:13:7d:68"; ip-address = "172.16.1.12"; hostname = "tofu"; }
                            #{ hw-address = "00:0d:b9:4c:21:b0"; ip-address = "172.16.1.16"; hostname = "apu2"}
                            { hw-address = "c4:dd:57:20:f4:1d"; ip-address = "172.16.1.21"; hostname = "prokord1"; }
                            { hw-address = "c4:dd:57:20:f2:88"; ip-address = "172.16.1.22"; hostname = "prokord2"; }
                            { hw-address = "0c:96:e6:59:61:df"; ip-address = "172.16.1.51"; hostname = "sonybraviatv"; }
                            { hw-address = "50:ec:50:0d:b6:fd"; ip-address = "172.16.1.52"; hostname = "bimble"; }
                        ];
                    }
                    {
                        subnet = "172.16.66.0/24";
                        pools = [{ pool = "172.16.66.100 - 172.16.66.240"; }];
                        option-data = [
                            { name = "routers"; data = "172.16.66.1"; }
                            { name = "domain-name-servers"; data = "1.0.0.1, 1.1.1.1"; }
                        ];
                    }];
                };
            };
        };
    };
}
