{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.duckdns;
in {
    options.ronnvall.duckdns = {
        enable = mkEnableOption "zfs enabled";
        domain = mkOption {
            default = "floodiboob.duckdns.org";
        };
    };
    config = mkIf cfg.enable {
        systemd.services.duckdns_update = {
            serviceConfig.Type = "oneshot";
            script = ''
                #!${pkgs.bash}/bin/bash
                DOMAIN="${cfg.domain}"
                TOKEN="4913a016-7edb-4735-a672-88479e3c15bf"
                echo "Updating duckdns"
                ${pkgs.curl}/bin/curl -sSv "https://www.duckdns.org/update?domains=$DOMAIN&token=$TOKEN&ip="
            '';
        };
        systemd.timers.duckdns_update = {
            wantedBy = [ "timers.target" ];
            partOf = [ "duckdns_update.service" ];
            timerConfig.OnCalendar = "hourly";
        };
    };
}
