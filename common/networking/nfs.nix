{ lib, ... }:{
    config = {
        system.activationScripts.makeNFSMount = lib.stringAfter [ "var" ] ''
        mkdir -p /vol/nfs/burrito/media/{film,tv,music}
        mkdir -p /vol/nfs/burrito/vol/backup
        '';

        fileSystems."/vol/nfs/burrito/media/film" = {
            device = "burrito:/media/film";
            fsType = "nfs";
            options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
        };
        fileSystems."/vol/nfs/burrito/media/tv" = {
            device = "burrito:/media/tv";
            fsType = "nfs";
            options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
        };
        fileSystems."/vol/nfs/burrito/media/tv_sorted" = {
            device = "burrito:/media/tv_sorted";
            fsType = "nfs";
            options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
        };
        fileSystems."/vol/nfs/burrito/media/music" = {
            device = "burrito:/media/music";
            fsType = "nfs";
            options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
        };
        fileSystems."/vol/nfs/burrito/vol/backup" = {
            device = "burrito:/vol/backup";
            fsType = "nfs";
            options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ];
        };
    };
}

