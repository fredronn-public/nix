{
    imports = [
        ./nftables.nix
        ./bird.nix
        ./containerbridge.nix
        ./duckdns.nix
        ./wifi.nix
        ./nfs.nix
        ./dnsmasq.nix
        ./wol.nix
        ./tailscale.nix
        ./nebula.nix
    ];
    config = {
        services.resolved.dnssec = "false";
        services.lldpd.enable = true;
        services.chrony.enable = true;
        networking.extraHosts = ''
        10.200.101.76 apu2
        10.200.101.75 ampere
        10.200.101.74 lengue
        10.200.101.73 churro
        10.200.101.72 paddington
        10.200.101.71 nixberry
        10.200.101.70 chalupa
        10.200.101.69 cyberpunk
        10.200.101.68 salsa
        10.200.101.67 apu1
        10.200.101.66 burrito nextcloud gitlab
        10.200.101.65 nuc hass influx influx.external postgres postgres.external ldap ldap.external 
        10.200.101.225 churro
        10.200.101.233 apu1
        10.200.101.232 apu2
        10.200.101.237 nixiso
        10.200.101.241 cloudberrypi
        10.200.101.244 apu2
        10.200.101.245 ampere
        
        172.16.1.11 nuc
        172.16.1.12 tofu
        62.63.229.149 apu.ronnvall.com apu
        '';
    };
}
