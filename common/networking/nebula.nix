{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.ronnvall.nebula;
  hostName = config.networking.hostName;
in
{
  options.ronnvall.nebula = {
    enable = mkOption { default = false; };
    lightHouses = mkOption { default = ["salsa" "ampere"]; };
    staticHostMap = mkOption { default = {}; };
  };
  config = mkIf cfg.enable {
    sops.secrets."nebula/ca".sopsFile = ./nebula.yaml;
    sops.secrets."nebula/cert/${hostName}".sopsFile = ./nebula.yaml;
    services.nebula.networks.default = {
      enable = true;
      isLighthouse = elem hostName cfg.lightHouses;
      ca = config.sops.secrets."nebula/ca".path;
      #cert = config.sops.secrets."nebula/cert/${hostName}".path;
      staticHostMap = cfg.staticHostMap;
    };
  };
}