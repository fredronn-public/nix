# Defines NFTABLES firewall, must be included

{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.nftables;
in {
    options.ronnvall.nftables = {
        enable = mkOption {
            default = true;
        };
        allowedNets = mkOption {
            default = [ "10.0.0.0/8" "172.16.0.0/12" "192.168.0.0/16" ]; # RFC1918
        };
        allowedInterfaces = mkOption {
            default = [ "lan" ];
        };
        isolatedInterfaces = mkOption {
            default = [ "isolated" ];
        };
        denyInternetInterfaces = mkOption {
            default = [ "mgmt" "iot" ];
        };
        allowedProto = mkOption {
            default = [ "ospfigp" ];
        };
        allowedPorts = mkOption {
            default = [ ];
        };
        allowedTCPPorts = mkOption {
            default = [ ];
        };
        allowedUDPPorts = mkOption {
            default = [ ];
        };
        natPorts = mkOption {
            default = [ ];
        };
        noNatPorts = mkOption {
            default = [ ];
        };
    };

    config = mkIf cfg.enable {
      services.openssh.settings.LogLevel = "VERBOSE";
      services.fail2ban = {
        enable=true;
        packageFirewall=pkgs.nftables;
        banaction="nftables-multiport";
        ignoreIP=cfg.allowedNets;
        bantime-increment.enable = true;
        bantime-increment.overalljails = true;
      };
      networking = {
        nat.enable = false;
        firewall.enable = false;
        nftables = {
          enable = true;
          ruleset = ''

define RFC1918 = { 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16 }

table inet filter {
  chain input {
    type filter hook input priority filter - 1; policy drop;
    ct state invalid drop comment "early drop of invalid connections"
    ct state { established, related } accept comment "allow tracked connections"
    iifname "lo" accept comment "allow from loopback"
    icmpv6 type {echo-request,nd-neighbor-solicit,nd-neighbor-advert,nd-router-solicit,
             nd-router-advert,mld-listener-query,destination-unreachable,
             packet-too-big,time-exceeded,parameter-problem} accept

    iifname { "br-container", "br-lan" } udp dport { 53, 67, 68 } accept comment "Allow dns/dhcp"
    iifname "virbr*" udp dport { 53, 67, 68 } accept comment "Allow dns/dhcp"

    ${optionalString (cfg.allowedInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.allowedInterfaces} } accept comment "Allow Interfaces"
    ''}
    
    ${optionalString (cfg.isolatedInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.isolatedInterfaces} } udp dport 67 accept comment "Allow DHCP"
    iifname { ${concatStringsSep "," cfg.isolatedInterfaces} } ip daddr $RFC1918 drop comment "Drop the rest"
    ''}

    ${optionalString (cfg.denyInternetInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.denyInternetInterfaces} } udp dport 67 accept comment "Allow DHCP"
    iifname { ${concatStringsSep "," cfg.denyInternetInterfaces} } drop comment "Drop the rest"
    ''}

    ${optionalString (cfg.allowedNets != [ ]) ''
    ip saddr { ${concatStringsSep "," cfg.allowedNets} } accept comment "Allow NETS"
    ''}
    ${optionalString (cfg.allowedProto != [ ]) ''
    ip protocol { ${concatStringsSep "," cfg.allowedProto} } accept comment "Allow Protocols"
    ''}
    ${optionalString (cfg.allowedTCPPorts != [ ]) ''
    tcp dport { ${concatStringsSep "," cfg.allowedTCPPorts} } accept comment "Allow TCP"
    ''}
    ${optionalString (cfg.allowedUDPPorts != [ ]) ''
    udp dport { ${concatStringsSep "," cfg.allowedUDPPorts} } accept comment "Allow UDP"
    ''}
    ${optionalString (cfg.allowedPorts != [ ]) ''
    ip meta l4proto { tcp, udp } th dport { ${concatStringsSep "," cfg.allowedPorts} } accept comment "Allow TCP/UDP"
    ''}
  }

  chain forward {
    type filter hook forward priority filter - 1; policy drop;
    ct state invalid drop comment "early drop of invalid connections"
    ct state { established, related } accept comment "allow tracked connections"
    
    ${optionalString (cfg.isolatedInterfaces != [ ] && cfg.natPorts != []) ''
    iifname { ${concatStringsSep "," cfg.isolatedInterfaces} } oifname { ${concatStringsSep "," cfg.natPorts} } accept comment "Allow NAT Out"
    ''}
    
    ${optionalString (cfg.isolatedInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.isolatedInterfaces} } drop
    ''}

    ${optionalString (cfg.denyInternetInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.denyInternetInterfaces} } drop
    ''}

    ${optionalString (cfg.allowedTCPPorts != [ ]) ''
    tcp dport { ${concatStringsSep "," cfg.allowedTCPPorts} } accept comment "Allow TCP"
    ''}

    ${optionalString (cfg.allowedUDPPorts != [ ]) ''
    udp dport { ${concatStringsSep "," cfg.allowedUDPPorts} } accept comment "Allow UDP"
    ''}

    ${optionalString (cfg.allowedInterfaces != [ ]) ''
    iifname { ${concatStringsSep "," cfg.allowedInterfaces} } accept comment "allow these interfaces"
    ''}

    ip saddr { ${concatStringsSep "," cfg.allowedNets} } counter accept
  }
}

${optionalString (cfg.natPorts != [ ]) ''
table inet nat {
    chain postrouting {
      type nat hook postrouting priority 100; policy accept;
      ${flip concatMapStrings cfg.natPorts (iface: ''
      oifname "${iface}" masquerade
      '')}
    }
}''}

table ip filter {
  chain DOCKER-USER {
      accept
  }
}
'';
        };
    };
};
}
