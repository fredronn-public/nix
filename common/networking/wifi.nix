{ lib, pkgs, config, ... }:
with lib;
let
    cfg = config.ronnvall.wifi;
    wired_wifi_script = pkgs.writeText "70-wifi-wired-exclusive.sh" ''
        #!${pkgs.bash}/bin/bash
        export LC_ALL=C
        NMCLI=${pkgs.networkmanager}/bin/nmcli
        GREP=${pkgs.gnugrep}/bin/grep
        
        enable_disable_wifi ()
        {
            result=$($NMCLI dev | $GREP "ethernet" | $GREP -w "connected")
            if [ -n "$result" ]; then
                $NMCLI radio wifi off
            else
                $NMCLI radio wifi on
            fi
        }
        
        if [ "$2" = "up" ]; then
            enable_disable_wifi
        fi
        
        if [ "$2" = "down" ]; then
            enable_disable_wifi
        fi
    '';

in {
    options.ronnvall.wifi = {
        enable = mkOption { default = false; description = "WiFi enabled, will enable NetworkManager"; };
        fiveg = mkOption {
            description = "Explicitly only connect to 5GHz connections";
            default = true;
        };
    };

    config = mkIf cfg.enable {
        nixpkgs.config.allowUnfree = true;
        hardware.enableAllFirmware = true;
        networking.wireless.enable = false;
        networking.networkmanager.enable = true;
        networking.networkmanager.dispatcherScripts = [
            { source = wired_wifi_script; type = "basic"; }
        ];
        networking.networkmanager.extraConfig = ''
        [connection]
        ethernet.wake-on-lan = magic
        wifi.wake-on-wlan = magic
        '';

        environment.systemPackages = with pkgs; [
            iw
            wirelesstools
        ];
        sops.secrets.eduroam.sopsFile = ./wifi.yaml;
        sops.secrets.hummus.sopsFile = ./wifi.yaml;
        sops.secrets."Ronnvall Telia".sopsFile = ./wifi.yaml;
        sops.secrets."hummus-portable".sopsFile = ./wifi.yaml;
        environment.etc."NetworkManager/system-connections/eduroam.nmconnection".source = config.sops.secrets.eduroam.path;
        environment.etc."NetworkManager/system-connections/hummus.nmconnection".source = config.sops.secrets.hummus.path;
        environment.etc."NetworkManager/system-connections/Ronnvall Telia.nmconnection".source = config.sops.secrets."Ronnvall Telia".path;
        environment.etc."NetworkManager/system-connections/hummus-portable.nmconnection".source = config.sops.secrets."hummus-portable".path;

        hardware.wirelessRegulatoryDatabase = true;
        boot.extraModprobeConfig = ''
        options cfg80211 ieee80211_regdom=SE
        '';
    };
}
