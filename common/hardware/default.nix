{ lib, config, ... }:
let
  rtw89 = config.boot.kernelPackages.callPackage ./rtw89.nix {};
  cfg = config.ronnvall.hardware;
in
{
    options = {
        ronnvall.hardware.rtw89.enable = lib.mkOption { default = false; };
    };
    config = lib.mkMerge [
        # If rtw89 should be included
        (lib.mkIf cfg.rtw89.enable {
            boot.extraModulePackages = with config.boot.kernelPackages; [ rtw89 ];
            boot.kernelModules = [ "rtw_8852be" ];
        })
    ];
}
