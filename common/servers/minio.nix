{ pkgs, lib, config, ... }:
with lib;
let
    cfg = config.ronnvall.minio;
    zfs_enable = config.ronnvall.zfs.enable;
    zroot = config.ronnvall.zfs.zroot;
in
{
    options.ronnvall.minio = {
        enable = mkEnableOption "minio";
    };
    config = mkIf cfg.enable (mkMerge [
        { services.minio.enable = true; ronnvall.restic.backupPaths = [ "/var/lib/minio" ]; }
        (mkIf zfs_enable {
            systemd.services.minio = {
                wants = ["minio_zfs_create.service"];
                after = ["minio_zfs_create.service"];
            };
            systemd.services.minio_zfs_create = {
                script = ''
                    if ! ${pkgs.zfs}/bin/zfs list ${zroot}/var/lib/minio; then
                        ${pkgs.zfs}/bin/zfs create ${zroot}/var/lib/minio -o canmount=on
                    fi
                    chown -R minio:minio /var/lib/minio
                '';
            };
        })]
    );
}
