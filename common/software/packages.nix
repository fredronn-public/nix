{ pkgs, ... }:
with pkgs;
let
    wgnord = callPackage ./wgnord.nix {};
in
{
    config = {
        environment.systemPackages = with pkgs; [
            tshark
            dnsutils
            nnn
            neovim
            wgnord
            dua
            home-manager
	        git
            dmidecode
            lshw
        ];
        programs.tmux = {
            enable = true;
            clock24 = true;
            extraConfig = ''
                unbind C-b
                set -g prefix C-a
            '';
        };
        system.activationScripts = {
            etc_wireguard = ''
                mkdir -p /etc/wireguard
            '';
        };
    };
}
