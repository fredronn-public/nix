{ config, lib, pkgs, ... }:
{
    config = {
        environment.systemPackages = with pkgs; [
            (vscode-with-extensions.override {
              vscode = code-server;
              vscodeExtensions = with vscode-extensions; [
                ms-python.python
              ];
            })
        ];
    };
}
