{ lib, pkgs, config, ... }:
let
    cfg = config.ronnvall.graphical;
in
{
    config = lib.mkIf (cfg.enable && cfg.variant != "hyprland") {
        environment.systemPackages = with pkgs; [ arandr ];
        services.autorandr.enable = true;
   };
}
