{pkgs, config, lib, ...}: let
  cfg = config.ronnvall.graphical;
  flake-compat = builtins.fetchTarball "https://github.com/edolstra/flake-compat/archive/master.tar.gz";

  hyprland = (import flake-compat {
    src = builtins.fetchTarball "https://github.com/hyprwm/Hyprland/archive/master.tar.gz";
  }).defaultNix;

  hybridbar = pkgs.callPackage (import ./hybridbar.nix) {};
in {
  imports = [hyprland.nixosModules.default];
    
  config = lib.mkIf (cfg.variant == "hyprland" && cfg.enable ) {
    services.xserver.enable = true;
    services.xserver.displayManager.gdm.enable = true;
    environment.systemPackages = with pkgs; [ wofi kanshi hybridbar ];
    programs.hyprland = {
      enable = true;

      # default options, you don't need to set them
      package = hyprland.packages.${pkgs.system}.default;

      xwayland = {
        enable = true;
        hidpi = true;
      };

      nvidiaPatches = false;
    };
  };
}
