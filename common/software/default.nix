{ pkgs, ... }:{
    imports = [ ./packages.nix ./graphical.nix ./autorandr.nix ];
}
