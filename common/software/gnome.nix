{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.graphical;
in {
    config = mkIf (cfg.variant == "gnome" && cfg.enable ) {
        environment.gnome.excludePackages = with pkgs; [ gnome.gnome-software ];
        environment.systemPackages = with pkgs; [
          gnome.gnome-tweaks
          gnomeExtensions.dash-to-dock
        ];
        programs.gnome-terminal.enable = true;
        services.xserver.enable = true;
        services.xserver.displayManager.gdm.enable = true;
        services.xserver.desktopManager.gnome.enable = true;
        security.pam.services.login.enableGnomeKeyring = true;
        services.gnome.gnome-keyring.enable = true;
    };
}
