{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.graphical;
in {
    config = mkIf (cfg.variant == "i3" && cfg.enable ) {
        xdg.portal = {
            enable = true;
            extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
        };
        services.blueman.enable = true;
        services.xserver.enable = true;
        services.xserver.libinput = {
            enable = true;
            touchpad = {
                naturalScrolling = true;
                middleEmulation = true;
                tapping = true;
            };
        };
        services.xserver.displayManager = {
            defaultSession = "none+i3";
            lightdm.enable = true;
            lightdm.greeters.enso.enable = true;
        };
        services.xserver.windowManager.i3.enable = true;
        services.xserver.windowManager.i3.package = pkgs.i3-gaps;
        services.xserver.windowManager.i3.extraPackages = with pkgs; [
            xsettingsd i3status-rust light
        ];
        services.xserver.windowManager.i3.extraSessionCommands = ''
            export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
            gpgconf --launch gpg-agent
        '';
    };
}
