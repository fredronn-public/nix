{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.graphical;
  systemType = config.ronnvall.system.type;
in {
    options.ronnvall.graphical = {
        enable = mkOption { default = false; };
        variant = mkOption { default = "i3"; };
    };
    imports = [
        ./autorandr.nix
        ./gnome.nix
        ./i3.nix
        ./hyprland.nix
    ];

    config = mkIf cfg.enable {
        services.gnome.gnome-keyring.enable = mkDefault true;
        services.flatpak.enable = true;
        services.gvfs.enable = true;
        programs.dconf.enable = true;

        services.upower.enable = true;
        services.dbus.enable = true;
        programs.steam.enable = true;
        programs.light.enable = true;
        xdg.portal.wlr.enable = true;
       
        hardware.nvidia.modesetting.enable = true;
        fonts = {
            enableDefaultFonts = false;
            fontDir.enable = true;
            fonts = with pkgs; [
                inconsolata
                hack-font
                roboto
                ubuntu_font_family
                fira-code
                proggyfonts
                noto-fonts
            #  (nerdfonts.override { fonts = ["Hack" "FiraCode" "Inconsolata" "Noto"]; })
            ];
        };
        services.xserver.layout = "se";
        hardware.bluetooth.enable = true;
        hardware.pulseaudio.enable = false;
        services.pipewire = {
            enable = true;
            alsa.enable = true;
            alsa.support32Bit = true;
            pulse.enable = true;
        };
        hardware.bluetooth.settings = {
            General = {
                Enable = "Source,Sink,Media,Socket";
            };
        };

        systemd.services.flatpak_update = {
            serviceConfig.Type = "oneshot";
            script = ''
                echo "Updating flatpaks"
                ${pkgs.flatpak}/bin/flatpak update --assumeyes
            '';
        };
        systemd.timers.flatpak_update = {
            wantedBy = [ "timers.target" ];
            partOf = [ "flatpak_update.service" ];
            timerConfig.OnCalendar = "hourly";
        };

    };
}
