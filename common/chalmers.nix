{ pkgs, config, lib, ... }:
with lib;
let
  cfg = config.ronnvall.chalmers;
  graphical = config.ronnvall.graphical.enable;
in {
    options.ronnvall.chalmers = {
        enable = mkOption {
            default = false;
        };
    };
    config = mkIf cfg.enable {
        krb5.enable = true;
        krb5.libdefaults = {
            default_realm = "CHALMERS.SE";
            clockskew = "300";
            v4_instance_resolve = false;
            dns_lookup_kdc = true;
        };
        krb5.domain_realm = {
            ".chalmers.se" = "CHALMERS.SE";
            "chalmers.se" = "CHALMERS.SE";
        };
        services.printing.enable = true;
        services.printing.clientConf = ''
        ServerName lprint1.ita.chalmers.se
        '';

        environment.systemPackages = mkIf graphical (with pkgs; [ sstp networkmanager-sstp teams remmina zoom-us ]);
    };
}
