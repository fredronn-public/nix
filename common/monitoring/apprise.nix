{ pkgs, config, ... }:{
    config = {
        sops.secrets."apprise_targets".sopsFile = ./apprise.yaml;
        environment.etc."apprise.conf".source = config.sops.secrets."apprise_targets".path;
        environment.systemPackages = with pkgs; [ apprise ];
    };
}
