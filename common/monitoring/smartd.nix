{pkgs, lib, config, ...}:
let
  smartNotify = pkgs.writeScript "smartd-notify-apprise.sh" ''
#!${pkgs.bash}/bin/bash
APPRISE="${pkgs.apprise}/bin/apprise"
${pkgs.coreutils}/bin/cat << EOF | $APPRISE -c /etc/apprise.conf
Problem detected with disk: $SMARTD_DEVICESTRING
Warning message from smartd is:
$SMARTD_FULLMESSAGE
EOF
  '';
in
{
    config = {
        services.smartd.enable = true;
        services.smartd.defaults.autodetected = "-M exec ${smartNotify} -a -o on -s (S/../.././02|L/../../7/04)";
        services.smartd.notifications.test = false;
        environment.systemPackages = with pkgs; [
          smartmontools
        ];
    };
}
