{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.ronnvall.telegraf;
in {
    options.ronnvall.telegraf = {
        enable = mkOption { default = true; };
    };

    config = mkIf cfg.enable {
        users.users.telegraf.extraGroups = [ "docker" ];
        services.telegraf = {
            enable = true;
            extraConfig = {
                agent = {
                    interval = "10s";
                };
                inputs = {
                    cpu = {
                        percpu = true;
                        totalcpu = true;
                        collect_cpu_time = true;
                        report_active = false;
                    };
                    disk.ignore_fs = [ "tmpfs" "devfs" "devtmpfs" "iso9660" "overlay" "aufs" "squashfs" ];
                    diskio = {};
                    kernel = {};
                    mem = {};
                    processes = {};
                    swap = {};
                    system = {};
                    temp = {};
                    zfs = {};
                    net = {
                        interfaces = [ "e*" "wl*" "wg*" "WAN" "br-lan" "br-container"];
                    };
                    ping = {
                        urls = [ "1.1.1.1" "8.8.8.8" "129.16.1.53" "129.16.2.53"];
                        #binary = "${pkgs.inetutils}/bin/ping";
                        method = "native";
                    };
                };
                outputs = {
                    influxdb = {
                        urls = [ "http://influx:8086" ];
                        database = "telegraf";
                        write_consistency = "any";
                        timeout = "10s";
                    };
                };
            };
        };
    };
}
