{ pkgs, lib, config, ... }:
with lib;
let
    cfg = config.ronnvall.k3s;
    zroot = config.ronnvall.zfs.zroot;
    zfs = config.ronnvall.zfs.enable;
    hostName = config.networking.hostName;
    role = (if (builtins.elem hostName cfg.controllers) then "server" else "agent");

    k3s = pkgs.k3s;
    containerd-reset-node = pkgs.writeScriptBin "containerd-reset-node" ''
        #!/bin/sh
        # THIS IS DANGEROUS BECAUSE IT DELETES SECRETS/CERTS e.g. in /var/lib/rancher.
        set -eux -o pipefail
        shopt -s nullglob
        systemctl stop containerd k3s
        find /sys/fs/cgroup/systemd/system.slice/containerd.service* /sys/fs/cgroup/systemd/kubepods* /sys/fs/cgroup/kubepods* -name cgroup.procs | \
            xargs -r cat | xargs -r kill -9
        mount | awk '/\/var\/lib\/kubelet|\/run\/netns|\/run\/containerd/ {print $3}' | xargs -r umount
        umount /var/lib/containerd
        zfs destroy -R ${zroot}/var/lib/containerd
        zfs create -V 50G ${zroot}/var/lib/containerd
        mount /var/lib/containerd
    '';
    k3s-reset-node = pkgs.writeScriptBin "k3s-reset-node" ''
        #!/bin/sh
        # THIS IS DANGEROUS BECAUSE IT DELETES SECRETS/CERTS e.g. in /var/lib/rancher.
        set -eux -o pipefail
        shopt -s nullglob
        systemctl stop containerd k3s
        find /sys/fs/cgroup/systemd/system.slice/containerd.service* /sys/fs/cgroup/systemd/kubepods* /sys/fs/cgroup/kubepods* -name cgroup.procs | \
            xargs -r cat | xargs -r kill -9
        mount | awk '/\/var\/lib\/kubelet|\/run\/netns|\/run\/containerd/ {print $3}' | xargs -r umount
        zfs destroy -R ${zroot}/var/lib/containerd
        zfs destroy -R ${zroot}/var/lib/rancher
    '';
    k3s-resolvconf = pkgs.writeText "resolv.conf" ''
        nameserver 1.1.1.1
        nameserver 8.8.8.8
        nameserver 9.9.9.9
    '';

in
{
    options.ronnvall.k3s = {
        enable = mkEnableOption "k3s";
        controllers = mkOption { default = [  "ampere" "apu1" "apu2" ]; };
        gc_low_threshold = mkOption { default = "40"; };
        gc_high_threshold = mkOption { default = "50"; };
        extra_tls = mkOption { default = "${hostName}"; };
    };

    config = mkIf cfg.enable {
        ronnvall.nftables.allowedInterfaces = [ "cni0" "flannel" "flannel.1" "bird1" ];
        ronnvall.restic.backupPaths = [ "/var/lib/rancher" ];
        fileSystems."/var/lib/containerd" = mkIf zfs { device = "/dev/${zroot}/var/lib/containerd"; fsType = "xfs"; options = [ "noatime" "discard" ]; };
        
        boot.kernelModules = [ "ceph" "rbd" "nbd" ];
        environment.systemPackages = with pkgs; [
            containerd-reset-node k3s-reset-node cni-plugins openiscsi
        ];
        sops.secrets."k3s/server_token" = {};
        sops.secrets."k3s/node_password" = {};
        sops.secrets."k3s/kubeconfig" = {};
        environment.etc."rancher/node/password".source = config.sops.secrets."k3s/node_password".path;
        environment.etc."kube/config".source = config.sops.secrets."k3s/kubeconfig".path;

        ## Need this for longhorn
        #services.openiscsi.enable = true;
        #services.openiscsi.name = "iqn.2020-08.org.linux-iscsi.initiatorhost:${hostName}";
        
        system.activationScripts = {
            mkdir_cni = ''
                if [ -e /opt/cni/bin ]; then
                    rm -rf /opt/cni/bin
                fi
                mkdir -p /opt/cni/bin && chmod a-w /opt/cni/bin
                cd /opt/cni/bin
                ln -sf ${pkgs.cni-plugins}/bin/* ${pkgs.cni-plugin-flannel}/bin/* ${pkgs.multus-cni}/bin/* .
            '';

            #symlink_longhorn = ''
            #    cd /bin
            #    ln -sf ${pkgs.util-linux}/bin/* .
            #    ln -sf ${pkgs.coreutils-full}/bin/* .
            #    ln -sf ${pkgs.openiscsi}/bin/* .
            #    ln -sf ${pkgs.bash}/bin/bash .
            #'';
        };

        virtualisation.containerd = {
            enable = true;
            settings = {
                version = 2;
                plugins."io.containerd.grpc.v1.cri".containerd.snapshotter = "overlayfs";
                plugins."io.containerd.grpc.v1.cri".cni = {
                    bin_dir = "${pkgs.runCommand "cni-bin-dir" {} ''
                        mkdir -p $out
                        ln -sf ${pkgs.cni-plugins}/bin/* ${pkgs.cni-plugin-flannel}/bin/* ${pkgs.multus-cni}/bin/* $out
                    ''}";
                    conf_dir = "/var/lib/rancher/k3s/agent/etc/cni/net.d/";
                };
            };    
        };

        services.k3s = {
            enable = true;
            role = "${role}";
            package = k3s;
            serverAddr = lib.mkIf (hostName != (head cfg.controllers) ) "https://${head cfg.controllers}:6443";
            tokenFile = config.sops.secrets."k3s/server_token".path;
            extraFlags = toString (if (role == "server") then [
                "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
                "--snapshotter overlayfs"
                "--disable traefik"
                "--disable metrics-server"
                "--disable servicelb"
                "--flannel-backend vxlan"
                "--flannel-iface bird1"
                "--node-ip ${config.ronnvall.bird.routerId}"
                "--resolv-conf ${k3s-resolvconf}"
                "--kubelet-arg=image-gc-high-threshold=${cfg.gc_high_threshold}"
                "--kubelet-arg=image-gc-low-threshold=${cfg.gc_low_threshold}"
                "--advertise-address ${config.ronnvall.bird.routerId}"
                "--tls-san ${cfg.extra_tls}"
            ] else [
                "--container-runtime-endpoint unix:///run/containerd/containerd.sock"
                "--snapshotter overlayfs"
                "--flannel-iface bird1"
                "--node-ip ${config.ronnvall.bird.routerId}"
                "--resolv-conf ${k3s-resolvconf}"
                "--kubelet-arg=image-gc-high-threshold=${cfg.gc_high_threshold}"
                "--kubelet-arg=image-gc-low-threshold=${cfg.gc_low_threshold}"
            ]);
        };

        systemd.services.k3s = {
            wants = ["containerd.service"];
            after = ["containerd.service"];
            serviceConfig.ExecStartPre = mkIf zfs [
                "-${pkgs.zfs}/bin/zfs create ${zroot}/var/lib/rancher -o canmount=on"
                "-${pkgs.zfs}/bin/zfs snap ${zroot}/var/lib/rancher@empty"
            ];
        };
    };
}
