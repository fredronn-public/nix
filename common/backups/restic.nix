{ pkgs, config, lib, ... }:
with lib;
let
    defaultBackupArgs = [
        "--exclude-caches"
        "--exclude .local"
        "--exclude .cache"
        "--exclude .vscode-server"
        "--exclude .cargo"
        "--exclude .var"
        "--exclude Cache"
        "--exclude CachedData"
        "--exclude CachedExtensions"
        "--exclude .zfs"
        "--exclude .snapshots"
        "--exclude .mkv"
        "--exclude .avi"
        "--exclude .m4a"
        "--exclude .mp4"
        "--exclude .dmg"
        "--exclude .img"
    ];
    defaultPruneOpts = [ ];
    cfg = config.ronnvall.restic;
in
{
    options.ronnvall.restic = {
        enable = mkOption { default = true; };
        backupArgs = mkOption {
            default = defaultBackupArgs;
        };
        pruneOpts = mkOption {
            default = defaultPruneOpts;
        };
        backupPaths = mkOption {
            default = [ "/home" "/persist" ];
        };
        backupTargets = mkOption {
            default = [ "burrito" "apu2" ];
        };
    };

    config = mkIf cfg.enable {
        environment.etc."restic_generic.env" = {
            mode = "0400";
            text = "GOCS=20";
        };

        #ronnvall.restic.backupPaths = mkIf (lib.attrsets.hasAttrByPath [ "/persist" ] config.fileSystems) [ "/persist" ];
        
        environment.systemPackages = with pkgs; [
            restic
            rclone
        ];
        
        sops.secrets."restic/passwords/jotta".sopsFile = ./restic.yaml;
        sops.secrets."restic/passwords/burrito".sopsFile = ./restic.yaml;
        sops.secrets."restic/passwords/salsa".sopsFile = ./restic.yaml;
        sops.secrets."restic/passwords/apu2".sopsFile = ./restic.yaml;
        services.restic.backups = filterAttrs (n: v: elem n cfg.backupTargets) {
            jotta = {
                initialize = false;
                passwordFile = config.sops.secrets."restic/passwords/jotta".path;
                environmentFile = "/etc/restic_generic.env";
                paths = cfg.backupPaths;
                extraBackupArgs = cfg.backupArgs;
                pruneOpts = cfg.pruneOpts;
                repository = "rclone:jotta:restic";
                rcloneConfigFile = "/persist/restic/rclone.conf";
                timerConfig = {
                    OnCalendar = "23:05";
                    RandomizedDelaySec = "3h";
                };
            };
            burrito = {
                initialize = true;
                passwordFile = config.sops.secrets."restic/passwords/burrito".path;
                environmentFile = "/etc/restic_generic.env";
                paths = cfg.backupPaths;
                extraBackupArgs = cfg.backupArgs;
                pruneOpts = cfg.pruneOpts;
                repository = "rest:http://burrito:8000/";
                timerConfig = {
                    OnCalendar = "23:05";
                    RandomizedDelaySec = "3h";
                };
            };
            #salsa = {
            #    initialize = true;
            #    passwordFile = config.sops.secrets."restic/passwords/salsa".path;
            #    environmentFile = "/etc/restic_generic.env";
            #    paths = cfg.backupPaths;
            #    extraBackupArgs = cfg.backupArgs;
            #    pruneOpts = cfg.pruneOpts;
            #    repository = "rest:http://salsa:8000/";
            #    timerConfig = {
            #        OnCalendar = "23:05";
            #        RandomizedDelaySec = "3h";
            #    };
            #};
            apu2 = {
                initialize = true;
                passwordFile = config.sops.secrets."restic/passwords/apu2".path;
                environmentFile = "/etc/restic_generic.env";
                paths = cfg.backupPaths;
                extraBackupArgs = cfg.backupArgs;
                pruneOpts = cfg.pruneOpts;
                repository = "rest:http://apu2:8000/";
                timerConfig = {
                    OnCalendar = "23:05";
                    RandomizedDelaySec = "3h";
                };
            };

        };
    };
}
