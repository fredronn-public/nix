from setuptools import setup

with open('requirements.txt','r') as f:
    REQS=f.readlines()

setup(
    name='pymedia',
    version='0.1',
    install_requires=REQS,
    packages=[
        'pymedia'
    ],
    entry_points='''
        [console_scripts]
        pymedia=pymedia.cli:cli
    ''',
)


