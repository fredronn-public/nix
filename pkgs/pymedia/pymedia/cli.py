from typing import Optional
import typer
from pymediainfo import MediaInfo
import os
import sys
from pathlib import Path

cli = typer.Typer()

@cli.command()
def move(
        searchpath: str,
        basepath: str,
        debug: bool = os.getenv('DEBUG', 'False') == 'True'
        ):
    searchpath = Path("/media/tv/iplayer")
    basepath = Path("/media/tv")
    for root,dir,files in os.walk(searchpath):
        for file in files:
            if not file.endswith('mp4'): continue
            try:
                file = searchpath.joinpath(file)
                media_info = MediaInfo.parse(file)
                data = media_info.to_data()
                data = data['tracks'][0]
                #print(data)
                show = data['collection'].split(':')[0]
                destpath = basepath.joinpath(show)
                title = data['title']
                filename = f'{title}.mp4'
                if 'season' in data.keys():
                    season = data['season']
                    destpath = destpath.joinpath(f'Season {season}')
                if 'part' in data.keys():
                    episode = data['part']
                    filename = f'{episode}-{filename}'
                destpath.mkdir(parents=True, exist_ok=True)
                destfile = destpath.joinpath(filename)
                if not debug: file.rename(destfile)
                print(f'Moved to {destfile}')
            except Exception as e: print(f'{file} failed to parse: {e}')

if __name__ == "__main__":
    app()