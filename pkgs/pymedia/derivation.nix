{ lib, pkgs, python3Packages }: with python3Packages;
let
  sqlmodel = (buildPythonPackage rec {
    pname = "sqlmodel";
    version = "0.0.8";
    src = fetchPypi {
      inherit pname version;
      sha256 = "";
    };
    doCheck = false;
    propagatedBuildInputs = with pkgs.python310Packages; [
    ];
  });
in
buildPythonApplication {
  name = "pymedia";
  src = ./.;
  propagatedBuildInputs = with pkgs.python310Packages; [
    pymediainfo
    typer
    pydantic
    colorama
    shellingham
    rich
  ];
}