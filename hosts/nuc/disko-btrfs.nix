{ disks ? [ "/dev/disk/by-path/pci-0000:00:12.0-ata-1" ], ... }: {
  disko.devices = {
    disk = {
      x = {
        type = "disk";
        device = builtins.elemAt disks 0;
        content = {
          type = "table";
          format = "gpt";
          partitions = [
            {
              name = "ESP";
              start = "1MiB";
              end = "128MiB";
              fs-type = "fat32";
              bootable = true;
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            }
            {
              name = "root";
              start = "512MiB";
              end = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                subvolumes = {
                  "/rootfs" = {
                    mountpoint = "/";
                    mountOptions = [ "compress=lzo" ];
                  };
                  "/home" = {
                    mountOptions = [ ];
                  };
                  "/nix" = {
                    mountOptions = [ "compress=lzo" "noatime" ];
                  };
                  "/persist" = {
                    mountOptions = [ ];
                  };
                  "/srv" = { mountOptions = [ "compress=lzo" ]; };
                  "/var" = { mountOptions = [ "compress=lzo" ]; };
                  "/var/cache" = { mountOptions = [ "compress=lzo" ]; };
                  "/var/log" = { mountOptions = [ "compress=lzo" ]; };
                  "/var/lib" = { mountOptions = [ "compress=lzo" ]; };
                  "/var/lib/rancher" = { mountOptions = [ "compress=lzo" ]; };
                  "/var/lib/containerd" = { mountOptions = [ "compress=lzo" ]; };
                };
              };
            }
          ];
        };
      };
    };
  };
}
