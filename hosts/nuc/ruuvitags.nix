{ pkgs, stdenv, fetchgit, ... }:
let

ruuvitags2mqtt = stdenv.mkDerivation rec {
  name = "ruuvitags2mqtt";
  src = fetchgit {
    url = "https://gitlab.ronnvall.com/public_repositories/ruuvitags2mqtt.git";
  };
};
in {
    config = {
        systemd.services.ruuvitags2mqtt = {
            serviceConfig.Type = "oneshot";
            script = ''
                #!${pkgs.bash}/bin/bash
                ${ruuvitags2mqtt}/bin/ruuvitags2mqtt
            '';
        };
    };
}

