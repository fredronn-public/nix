{ modulesPath, lib, config, pkgs, ... }:{
    imports = [
        ./filesystems.nix
        ./containers.nix
        ./../../services/postgres.nix
        ./../../services/influxdb.nix
        ./../../services/openldap.nix
        (modulesPath + "/installer/scan/not-detected.nix")
    ];
    config = {
        systemd.services.systemd-networkd-wait-online.enable = lib.mkForce false; # This will fail
        systemd.services.NetworkManager-wait-online.enable = lib.mkForce false; # This will also fail
        ronnvall.wifi.enable = true;
        ronnvall.wifi.fiveg = true;
        ronnvall.wol.action_commands = lib.mkBefore [
            "${pkgs.kubectl}/bin/kubectl --kubeconfig /etc/kube/config drain --ignore-daemonsets --delete-emptydir-data ${config.networking.hostName}"
            "${pkgs.systemd}/bin/systemctl suspend"
        ];
        ronnvall.bird = { enable = true; routerId = "10.200.101.65"; ospfInterfaces = [ "eno1" ]; };
        boot.loader.systemd-boot.enable = true;
        boot.loader.efi.canTouchEfiVariables = false;
        # boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
        networking.hostName = "nuc";
        networking.hostId = "4ecbead5";
        boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "usb_storage" "sd_mod" "rtsx_pci_sdmmc" ];
        boot.initrd.kernelModules = [ ];
        boot.kernelModules = [ "kvm-intel" ];
        boot.extraModulePackages = [ ];
        #boot.kernelParams = [ "nomodeset" ];
        #time.timeZone = "UTC";
        ronnvall.btrfs.enable = true;
        ronnvall.restic = {
            enable = true;
            backupPaths = [
                "/home"
                "/srv"
                "/var/backup"
                "/persist"
            ];
            backupTargets = ["burrito" "jotta" "apu2"];
        };
        powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
        hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
        services.openssh.hostKeys = [
          { type = "rsa"; bits = 4096; path = "/persist/ssh_host_rsa_key"; }
          { type = "ed25519"; path = "/persist/ssh_host_ed25519_key"; }
        ];
        environment.systemPackages = with pkgs; [ screen compsize ];
        sops.age.sshKeyPaths = [ "/persist/ssh_host_ed25519_key" ];
    };
}

