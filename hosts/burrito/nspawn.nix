{ pkgs, ... }:{
    config = {
        systemd.nspawn = {
            "deluge" = {
                enable = true;
                networkConfig = {
                    Bridge="br-container";
                    Port="tcp:8112:8112";
                };
                filesConfig = {
                    Bind = [
                    "/media/downloads"
                    "/media/film"
                    "/media/tv"
                    ];
                };
                execConfig = { Boot = true; };
            };
            "arch" = {
                enable = false;
                networkConfig = {
                    Bridge="br-container";
                };
                filesConfig = {
                    Bind = [
                    "/media/downloads"
                    "/media/film"
                    "/media/tv"
                    ];
                };
                execConfig = {
                    Boot = true;
                };
            };

        };

        systemd.services."nspawn-start" = {
            enable = true;
            wantedBy = [ "multi-user.target" ];
            script = ''
            #!${pkgs.bash}/bin/bash
            machinectl start deluge
            #machinectl start arch
            '';
        };
    };
}
