{ config, lib, pkgs, modulesPath, ... }:{
    sops.secrets."wireguard/hosts/burrito/private".sopsFile = ./../../wireguard.yaml;
    sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
    ronnvall.nftables.allowedInterfaces = [ "wg-salsa" "wg-ampere" ];
    ronnvall.bird.ospfInterfaces = [ "wg-salsa" "wg-ampere" ];
    networking.wireguard.interfaces = {
        "wg-salsa" = {
            allowedIPsAsRoutes = false;
            ips = [ "10.200.101.5/31" ];
            privateKeyFile = config.sops.secrets."wireguard/hosts/burrito/private".path;
            peers = [{
                publicKey="lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
                allowedIPs=["0.0.0.0/0"];
                endpoint="62.63.229.149:51821";
                persistentKeepalive=10;
            }];
        };
        "wg-ampere" = {
            allowedIPsAsRoutes = false;
            ips = [ "10.200.101.247/31" ];
            privateKeyFile = config.sops.secrets."wireguard/hosts/burrito/private".path;
            peers = [{
                publicKey = "wnMqAGL3hgj9/KcqREgS8o+flR1DN/b3V2Kxl8juZSU=";
                allowedIPs = ["0.0.0.0/0"];
                endpoint="129.151.193.237:51821";
                persistentKeepalive=10;
            }];
        };
    };
}
