{ pkgs, lib, config, ... }:{
    config = {
        systemd.services = {
            uptime_jotta = {
                serviceConfig.Type = "oneshot";
                script = ''
                ${pkgs.curl}/bin/curl "https://status.ronnvall.com/api/push/KHPtBl1sdn?status=up&msg=OK"
                '';
            };
            uptime_salsa = {
                serviceConfig.Type = "oneshot";
                script = ''
                ${pkgs.curl}/bin/curl "https://status.ronnvall.com/api/push/iplJnzV2L9?status=up&msg=OK"
                '';
            };
            restic-backups-jotta.onSuccess = [ "uptime_jotta.service" ];
            restic-backups-salsa.onSuccess = [ "uptime_salsa.service" ];
        };
        ronnvall.restic = {
            enable = true;
            backupPaths = [
                "/home"
                "/srv"
                "/var/lib/machines"
                "/vol/dlink_nas"
                "/vol/backup"
                "/media/photos"
                "/media/music"
            ];
            backupTargets = [ "salsa" "jotta" ];
        };
        services.restic.server.enable = true;
        services.restic.server.appendOnly = true;
        services.restic.server.extraFlags = ["--no-auth"];
    };
}
