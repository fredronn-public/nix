{ pkgs, lib, config, ... }:{
    config = {
        systemd.services = {
            statping_jotta = {
                serviceConfig.Type = "oneshot";
                script = "${pkgs.curl}/bin/curl -k https://status.ronnvall.com:1043/checkin/ikxK3brUm8LtBPNpt2JmGTAgE5wElFLB";
            };
            statping_salsa = {
                serviceConfig.Type = "oneshot";
                script = "${pkgs.curl}/bin/curl -k https://status.ronnvall.com:1043/checkin/7NTAAOEyEmZpfhBlszPMN6cLpEouw49h";
            };
            restic-backups-jotta.onSuccess = "statping_jotta.service";
            restic-backups-salsa.onSuccess = "statping_salsa.service";
        };
        ronnvall.restic = {
            enable = true;
            backupPaths = [
                "/home"
                "/srv"
                "/var/lib/machines"
                "/vol/dlink_nas"
                "/vol/backup"
                "/media/photos"
                "/media/music"
            ];
            backupTargets = [ "salsa" "jotta" ];
        };
        services.restic.server.enable = true;
        services.restic.server.appendOnly = true;
        services.restic.server.extraFlags = ["--no-auth"];

    };
}
