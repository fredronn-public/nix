{ config, lib, pkgs, modulesPath, ... }:

{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot.initrd.availableKernelModules = [ "ahci" "usbhid" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" "wl" ];
  boot.extraModulePackages = [ ];

  boot.kernelParams = [ "console=tty1" "console=ttyS0,115200" ];

  fileSystems."/" = { device = "naspool/ROOT/rootfs"; fsType = "zfs"; };
  #fileSystems."/" = { device = "none"; fsType = "tmpfs"; options = [ "defaults" "size=128M" "mode=755" ]; };
  fileSystems."/tmp" = { device = "naspool/ROOT/tmp"; fsType = "zfs"; };
  fileSystems."/nix" = { device = "naspool/ROOT/nix"; fsType = "zfs"; };
  fileSystems."/persist" = { device = "naspool/userdata/persist"; fsType = "zfs"; neededForBoot = true; };
  fileSystems."/boot" = { device = "/dev/disk/by-id/ata-INTEL_SSDSA2M160G2GN_CVPO0276003W160AGN-part1"; fsType = "ext4"; };

  system.activationScripts.makeDlinkNasDir = lib.stringAfter [ "var" ] ''
    mkdir -p /vol/dlink_nas
  '';

  fileSystems."/vol/dlink_nas" = { device = "192.168.1.10:/mnt/HD_a2"; fsType = "nfs"; options = [ "x-systemd.automount" "noauto" "x-systemd.idle-timeout=600" ]; };

  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}

