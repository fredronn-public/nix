{
    config = {
        services.samba-wsdd.enable = true;
        services.samba = {
          enable = true;
          securityType = "user";
          extraConfig = ''
            workgroup = WORKGROUP
            server string = burrito
            netbios name = burrito
            security = user 
            #use sendfile = yes
            #max protocol = smb2
            # note: localhost is the ipv6 localhost ::1
            hosts allow = 10.200.101. 172.16.1. 192.168.1. 127.0.0.1 localhost
            hosts deny = 0.0.0.0/0
            guest account = nobody
            map to guest = bad user
          '';
          shares = {
            backup = {
              path = "/vol/backup";
              "valid users" = "fredrik";
              browseable = "yes";
              "read only" = "no";
              "guest ok" = "no";
              "create mask" = "0644";
              "directory mask" = "0755";
            };
          };
        };
    };
}
