{ pkgs, lib, ... }:{
    imports = [
        ./../../containers/deluge/default.nix
        ./../../containers/get_iplayer/default.nix
    ];
    config = {
        ronnvall.k3s = { enable = true; };
        fileSystems."/var/lib/containers" = { device = "/dev/naspool/var/lib/containers"; fsType = "xfs"; };
        fileSystems."/var/lib/rancher" = { device = "/dev/naspool/var/lib/rancher"; fsType = "xfs"; };
        virtualisation.podman.extraPackages = [ pkgs.zfs ];
        virtualisation.podman.enable = true;
    };
}
