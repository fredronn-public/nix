{ lib, config, pkgs, ... }:{
  nixpkgs.config.allowUnfree = true; 
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/disk/by-id/ata-INTEL_SSDSA2M160G2GN_CVPO0276003W160AGN";
  boot.loader.grub.extraConfig = "serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1; terminal_input serial; terminal_output serial";
  ronnvall.zfs.enable = true;
  ronnvall.zfs.persist = true;
  ronnvall.zfs.zroot = "naspool";

  boot.zfs.requestEncryptionCredentials=false;
  time.timeZone = "Europe/Stockholm";

  networking.hostName = "burrito"; # Define your hostname.
  networking.hostId = "736f3a67";
  networking.useDHCP = false;
  systemd.network.netdevs."br0" = {
    enable = true;
    netdevConfig = {
      Kind = "bridge";
      Name = "br0";
    };
  };
  systemd.network.networks."br0" = {
    enable = true;
    matchConfig = { Name="br0"; };
    networkConfig = { DHCP = "yes"; };
  };
  systemd.network.networks."switchdevs" = {
    enable = true;
    matchConfig = { Name="en*"; };
    networkConfig = { Bridge = "br0"; };
  };
  boot.initrd.postDeviceCommands = lib.mkAfter ''
    zfs rollback -r naspool/ROOT/rootfs@empty
    zfs rollback -r naspool/ROOT/tmp@empty
  '';
}
