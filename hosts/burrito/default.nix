{ pkgs, lib, config, ... }:{
    imports = [
        ./boot.nix
        ./hardware-configuration.nix
        ./wireguard.nix
        ./containers.nix
        ./restic.nix
    ];
    config = {
        ronnvall.bird = {
            enable = true;
            routerId = "10.200.101.66";
        };
        systemd.extraConfig = ''
            DefaultTimeoutStopSec=30s
        '';
        ronnvall.nftables.enable = true;
        ronnvall.nftables.allowedTCPPorts = [ "22" ];
        ronnvall.nftables.natPorts = [ "en*" "wl*" "br0" ];
        ronnvall.nftables.allowedInterfaces = [ "br-container" "cni-podman0" ] ;
        ronnvall.users.enable = true;
        ronnvall.autoUpdate.enable = true;
        ronnvall.autoUpdate.hostName = "burrito";
        services.containerbridge.enable = true;
        services.containerbridge.networkAddress = "172.18.2.1/24";
        services.redis.servers.default.enable = true;
        services.redis.servers.default.bind = null;
        services.redis.servers.default.port = 6379;
        services.redis.servers.default.settings = {
            protected-mode = "no";
        };
        ronnvall.duckdns = { enable = true; domain = "burritoboob.duckdns.org"; };
        services.nfs.server.enable = true;
        services.nfs.server.exports = ''
        /media/tv          172.16.0.0/12(ro) 10.200.101.0/24(ro)
        /media/tv_sorted   172.16.0.0/12(ro,crossmnt) 10.200.101.0/24(ro,crossmnt)
        /media/film        172.16.0.0/12(ro) 10.200.101.0/24(ro)
        /media/music       172.16.0.0/12(ro) 10.200.101.0/24(ro)
        /vol/backup        172.16.0.0/12(ro) 10.200.101.0/24(ro)
        /vol/backup/longhorn        172.16.0.0/12(rw) 10.0.0.0/8(rw)    
        /srv               172.16.0.0/12(rw,crossmnt) 10.200.101.0/24(rw,crossmnt)
        '';
        systemd.services.systemd-networkd-wait-online.enable = lib.mkForce false;
    };
}
