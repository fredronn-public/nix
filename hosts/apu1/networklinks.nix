{ pkgs, config, ... }:{
    config = {
        ronnvall.nftables = {
            enable = true;
            allowedTCPPorts = [ "22" ];
            allowedInterfaces = [ "br-lan" ];
            natPorts = [ "br-lan" ];
        };

        systemd.network = {
            networks = {
                "br-landevs" = {
                    enable = true;
                    matchConfig = {
                        Name="en*";
                    };
                    networkConfig = {
                        Bridge = "br-lan";
                    };
                };
                "br-lan" = {
                    name="br-lan";
                    enable = true;
                    matchConfig = {
                        Name="br-lan";
                    };
                    networkConfig = {
                        DHCP="yes";
                    };
                    dhcpConfig = {
                        RouteMetric="50";
                    };
                };
            };
            netdevs = {
                "br-lan" = {
                    enable = false;
                    netdevConfig = {
                        Name="br-lan";
                        Kind="bridge";
                    };
                };
            };
        };
    };
}


