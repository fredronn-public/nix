{ modulesPath, lib, config, pkgs, ... }:{
    imports = [
        ./../apu-shared
        ./wireguard.nix
        ./networklinks.nix
        (modulesPath + "/installer/scan/not-detected.nix")
    ];
    config = {
        systemd.services.systemd-networkd-wait-online.enable = lib.mkForce false; # This will fail
        systemd.services.NetworkManager-wait-online.enable = lib.mkForce false; # This will also fail
        ronnvall.k3s.enable = true;
        ronnvall.bird = { enable = true; routerId = "10.200.101.67"; };
        networking.hostName = "apu1";
        networking.hostId = "e21dc65c";
    };
}

