{ config, lib, pkgs, modulesPath, ... }:{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  nixpkgs.config.allowUnfree = true;

  services.xserver.videoDrivers = [ "nvidia" ];
  hardware.opengl.enable = true;

  # Optionally, you may need to select the appropriate driver version for your specific GPU.
  hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.beta;

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "usb_storage" "usbhid" "uas" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  boot.extraModulePackages = [ ];
  boot.loader = {
    systemd-boot.enable = true;
    efi = {
      canTouchEfiVariables = true;
      efiSysMountPoint = "/boot";
    };
  };

  fileSystems."/" = { device = "zroot/ROOT/rootfs"; fsType = "zfs"; };
  fileSystems."/nix" = { device = "zroot/ROOT/nix"; fsType = "zfs"; };
  fileSystems."/boot" = { device = "/dev/disk/by-id/nvme-Samsung_SSD_970_EVO_500GB_S5H7NS0N997391J-part5"; fsType = "vfat"; };
  fileSystems."/persist" = { device = "zroot/encrypt/persist"; fsType = "zfs"; neededForBoot = true; };

  swapDevices = [ ];

  zramSwap.enable = true;

  hardware.cpu.amd.updateMicrocode = true;
  services.autorandr.profiles.default.fingerprint = {
      "HDMI-1" = "00ffffffffffff001e6d5577ebce0800061e010380522278eea105a9564ba4250f5054210800d1c061404540010101010101010101011dbf7010d1a06f506840ca04345a3100001a000000fd0030551e8431000a202020202020000000fc004c472048445220575148440a20000000ff003030364e544641475a3235390a01070203447323090707830100004f61605f5e5d5a5922201f12100403016d030c001000b83c20006001020367d85dc401788003e30f0300e2006ae305c000e606050152525ee77c70a0d0a0295030203a00345a3100001a9d6770a0d0a0225030203a00345a3100001a0000000000000000000000000000000000000000000000c7";
  };
  services.autorandr.profiles.default.config."HDMI-1" = { mode = "3440x1440"; primary = true; };
}
