{ config, pkgs, lib, ... }:{
    imports = [ ./hardware-configuration.nix ];
    system.autoUpgrade.allowReboot = lib.mkForce false;
    ronnvall.graphical.enable = true;
    ronnvall.zfs.enable = true;
    ronnvall.zfs.persist = true;
    ronnvall.bird = { enable=true; routerId="10.200.101.70"; };
    ronnvall.wifi.enable = true;
    boot.binfmt.emulatedSystems = [ "aarch64-linux" ];
    boot.supportedFilesystems = [ "bcachefs" ];
    networking.hostName = "chalupa"; # Define your hostname.
    networking.hostId = "6cd0991d";
    # Set your time zone.
    time.timeZone = "Europe/Stockholm";
    networking.useDHCP = false;
    networking.networkmanager.enable = true;
    systemd.services.NetworkManager-wait-online.enable = false;
    #environment.systemPackages = with pkgs; [ "bcachefs-tools" ];
    boot.kernelParams = [ "ip=:::::enp6s0:dhcp" ]; # get dhcp on boot for zfs unlock
    boot.initrd = {
        postDeviceCommands = lib.mkAfter ''
            zfs rollback -r zroot/ROOT/rootfs@empty
            zfs rollback -r zroot/ROOT/tmp@empty
        '';

        network = {
            enable = true;
            ssh = {
                enable = true;
                hostKeys = [ /persist/ssh_host_ed25519_key ];
                authorizedKeys = [
                    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDp05qr/pza7Arbv2Oj2PrW1emJlXp8GAdxG6XA4Dyyh5fBuH6Wi36tODZ4jRvB3RwuBwDPC8Zi+eS+XqgiWadjBjO0R7E8zasq9dghBxnVq823JRUTr2qp5eNaV7tc4yFTfWTGxB/Oi73lL5ZKYuoj+t8OEhDoqxy5ygE+GyAbiPCbB+htQ66HhDoqhirEd1ZwbMdf0Qy85ESUlfAEXXJ/fDOuYg88ExqLZ+GxXblS6677YNVo8xcuPsDeeUfMBBPxFXUPhKYjnDpAaS8FxvXPT0eJ0SHiFYKnfQx6dkLapfHJBbiQ5XPDJxpwv9q1FZnJdHse1x1ArN20so6idj3v"
                    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOB3AAFiQCMp+mkhU1DFiSo8G2KMNRSHonH7nom4/Ffo"
                ];
            };
        };
    };
}

