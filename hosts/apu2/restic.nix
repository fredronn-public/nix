{ pkgs, lib, config, ... }:{
    config = {

        fileSystems."/var/lib/restic" = { device = "/dev/disk/by-label/seagate_expansion_8TB"; fsType = "btrfs"; options = [ "compress=zstd" "subvol=restic" ]; };
        services.restic.server.enable = true;
        services.restic.server.appendOnly = true;
        services.restic.server.extraFlags = ["--no-auth"];

        systemd.services = {
            uptime_jotta = {
                serviceConfig.Type = "oneshot";
                script = ''
                ${pkgs.curl}/bin/curl "https://status.ronnvall.com/api/push/yzkGJYJL6a?status=up&msg=OK"
                '';
            };
            uptime_burrito = {
                serviceConfig.Type = "oneshot";
                script = ''
                ${pkgs.curl}/bin/curl "https://status.ronnvall.com/api/push/QuQ00ZfQCX?status=up&msg=OK"
                '';
            };
            restic-backups-jotta.onSuccess = [ "uptime_jotta.service" ];
            restic-backups-burrito.onSuccess = [ "uptime_burrito.service" ];
        };
    };
}
