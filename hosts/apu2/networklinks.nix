
{ pkgs, config, ... }:{
    config = {
        networking.useDHCP = false;
        boot.kernel.sysctl."net.ipv4.ip_forward" = 1;
        services.udev.extraRules = ''
        ATTR{address}=="00:0d:b9:4c:21:b0", NAME="WAN"
        '';
        networking.interfaces.WAN.macAddress="00:e0:67:05:a9:f6";
        systemd.network = {
            links = {
                "WAN" = {
                    enable = true;
                    matchConfig = {
                        PermanentMACAddress="00:0d:b9:4c:21:b0";
                    };
                    linkConfig = {
                        Name="WAN";
                        MACAddress="00:e0:67:05:a9:f6";
                        Description="WAN Interface";
                    };
                };
            };
            networks = {
                "WAN" = {
                    name="WAN";
                    enable = true;
                    matchConfig = { Name="WAN"; };
                    networkConfig = { DHCP="yes"; ConfigureWithoutCarrier="yes"; };
                    dhcpV6Config = { PrefixDelegationHint = "::/56"; };
                    dhcpConfig = { RouteMetric="10"; };
                };
                "lan" = {
                    enable = true;
                    matchConfig = { Name="lan"; };
                    networkConfig = { ConfigureWithoutCarrier="yes"; };
                    address = [ "172.16.1.1/24" ];
                };
                "landevs" = {
                    enable = true;
                    matchConfig = { Name="en*"; };
                    networkConfig = { Bridge = "lan"; };
                };
            };
            netdevs = {
                "lan" = {
                    enable = true;
                    netdevConfig = {
                        Name="lan";
                        Kind="bridge";
                    };
                };
            };
        };
    };
}


