{ disks ? [ "/dev/disk/by-path/pci-0000:00:11.0-ata-1" ], ... }: {
  disko.devices = {
    disk = {
      x = {
        type = "disk";
        device = builtins.elemAt disks 0;
        content = {
          type = "table";
          format = "msdos";
          partitions = [
            {
              name = "boot";
              start = "0";
              end = "512MiB";
              #fs-type = "ext4";
              bootable = true;
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/boot";
              };
            }
            {
              name = "zfs";
              start = "512MiB";
              end = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            }
          ];
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        rootFsOptions = {
          compression = "zstd";
          "com.sun:auto-snapshot" = "false";
          mountpoint = "none";
        };
        datasets = {
          "ROOT" = {
            type = "zfs_fs";
          };
          "ROOT/nix" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
          };
          "ROOT/rootfs" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
            postCreateHook = "zfs snapshot zroot/ROOT/rootfs@rollback";
          };
          "ROOT/persist" = {
            type = "zfs_fs";
            options.mountpoint = "legacy";
          };
          "data"                = { type = "zfs_fs"; options.mountpoint = "none"; };
          "data/home"           = { type = "zfs_fs"; options.mountpoint = "/home"; };
          "data/home/fredrik"   = { type = "zfs_fs"; };
          "var" = { type = "zfs_fs"; options.canmount = "off"; options.mountpoint = "/var"; };
          "var/log" = { type = "zfs_fs"; };
          "var/log/journal" = { type = "zfs_fs"; options.mountpoint = "legacy"; };
          "var/lib" = { type = "zfs_fs"; };
          "var/lib/systemd" = { type = "zfs_fs"; options.canmount = "on"; };
        };
      };
    };
  };
}


