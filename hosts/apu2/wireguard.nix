{ config, lib, pkgs, modulesPath, ... }:
let
    hostName = config.networking.hostName;
in
{
    ronnvall.bird.ospfInterfaces = [ "wg-salsa" ];
    sops.secrets."wireguard/hosts/${hostName}/private".sopsFile = ./../../wireguard.yaml;
    sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
    networking.wireguard.interfaces = {
        "wg-salsa" = {
        allowedIPsAsRoutes = false;
        ips = [ "10.200.101.233/31" ];
        privateKeyFile = config.sops.secrets."wireguard/hosts/${hostName}/private".path;
        peers = [{
            publicKey="lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
            allowedIPs=["0.0.0.0/0"];
            endpoint="62.63.229.149:51825";
            persistentKeepalive=10;
        }];
        };
    };
}
