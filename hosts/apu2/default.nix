{ modulesPath, lib, config, pkgs, ... }:
let
  ageFile = pkgs.writeText "agekey.txt" (builtins.readFile ~/.config/sops/age/keys.txt);
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ./../apu-shared
    ./../salsa/wireguard.nix
    ./../../services/mqtt.nix
    ./networklinks.vlan.nix
    ./restic.nix
  ];
  config = {
    #systemd.services.systemd-networkd-wait-online.enable = lib.mkForce false; # This will fail
    #systemd.services.NetworkManager-wait-online.enable = lib.mkForce false; # This will also fail
    ronnvall.system.type = "server";
    ronnvall.graphical.enable = false;
    ronnvall.autoUpdate.enable = false;
    ronnvall.users.enable = true;
    ronnvall.wifi.enable = false;
    ronnvall.wifi.fiveg = false;
    ronnvall.telegraf.enable = true;
    ronnvall.k3s.enable = true;
    services.smartd.enable = lib.mkForce false;
    ronnvall.bird = { enable = true; routerId = "10.200.101.76"; };
    networking.hostName = "apu2";
    networking.hostId = "f4128b6e";
    boot.initrd.availableKernelModules = [ "ahci" "xhci_hcd" "xhci_pci" "xhci-pci-renesas" "usb_storage" ];
    boot.initrd.kernelModules = [ ];
    boot.kernelModules = [ "kvm-amd" ];
    boot.kernelParams = [ "amd_iommu=off" "usbcore.autosuspend=-1" "console=ttyS0,115200" ];
    time.timeZone = "UTC";
    networking.firewall.enable = false; 
    ronnvall.nftables = {
      enable = true;
      natPorts = [ "WAN" ];
      allowedTCPPorts = [ "22" "80" "443" "1883" "8883" ];
      allowedUDPPorts = [ "443" ];
      allowedInterfaces = [ "lan" "br-lan" "br-container" "bird1" ];
    };
    ronnvall.dhcp.enable = true;
    boot.loader.grub = {
      enable = true;
      device = "/dev/sda";
      extraConfig = "serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1; terminal_input serial; terminal_output serial";
    };
    ronnvall.journald.storage = "volatile";
    ronnvall.journald.size = "16M";
    powerManagement.cpuFreqGovernor = lib.mkDefault "performance";
    hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    environment.systemPackages = with pkgs; [ screen compsize ];
    sops.age.sshKeyPaths = [ "/persist/ssh_host_ed25519_key" ];


   fileSystems."/vol/seagate_expansion_8TB" = { device = "/dev/disk/by-label/seagate_expansion_8TB"; fsType = "btrfs"; options = [ "compress=zstd" ]; };
  };
}
