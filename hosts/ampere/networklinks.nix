{ pkgs, config, ... }:{
    config = {
        systemd.network = {
            networks = {
                "enp0s3" = {
                    name="enp0s3";
                    enable = true;
                    matchConfig = { Name="enp0s3"; };
                    networkConfig = { DHCP="yes"; };
                };
            };
        };
    };
}


