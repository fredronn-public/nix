{ config, pkgs, lib, ... }:with lib;{
    sops.secrets."wireguard/hosts/ampere/private".sopsFile = ./../../wireguard.yaml;
    sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
    ronnvall.bird.ospfInterfaces = [ "wg-salsa" "wg-burrito" ];
    networking.wireguard.interfaces = {
        "wg-salsa" = {
            ips = [ "10.200.101.245/31" ];
            allowedIPsAsRoutes = false;
            privateKeyFile = config.sops.secrets."wireguard/hosts/ampere/private".path;
            peers = [{
                publicKey = "lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
                allowedIPs = [ "0.0.0.0/0" ];
                endpoint = "62.63.229.149:51831";
                persistentKeepalive=10;
            }];
        };
        "wg-burrito" = {
            listenPort = 51821;
            allowedIPsAsRoutes = false;
            ips = [ "10.200.101.246/31" ];
            privateKeyFile = config.sops.secrets."wireguard/hosts/ampere/private".path;
            peers = [{
                publicKey = "akC00NL5hxSTEwf/lKeRLV1zUc5AjpBabZ6of1EuyCM=";
                allowedIPs = ["0.0.0.0/0"];
                persistentKeepalive=10;
            }];
        };
    };
    ronnvall.nftables.allowedUDPPorts = mapAttrsToList (name: value: "${toString value.listenPort}") config.networking.wireguard.interfaces;
}
