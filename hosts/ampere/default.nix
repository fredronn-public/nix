# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:
let
  externalIf = "enp0s3";
in
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wireguard.nix
      ./networklinks.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  ronnvall.graphical.enable = false;

  virtualisation.podman.enable = true;
  #virtualisation.podman.defaultNetwork.dnsname.enable = true;
  virtualisation.podman.defaultNetwork.settings.dns_enabled = true;

  #services.openssh.enable = true;
  #users.users.root.openssh.authorizedKeys.keys = [
  #  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOB3AAFiQCMp+mkhU1DFiSo8G2KMNRSHonH7nom4/Ffo"
  #  ];
  
  ronnvall.bird = { enable=true; routerId="10.200.101.75"; };
  ronnvall.k3s.enable = true;
  ronnvall.k3s.extra_tls = "10.200.101.245";
  ronnvall.nftables = {
    natPorts = [ "${externalIf}" ];
    allowedTCPPorts = [ "80" "443" ];
    allowedUDPPorts = [ "443" ];
  };

  ronnvall.restic.enable = true;

  networking.hostName = "ampere";
  networking.useDHCP = false;
  networking.interfaces."${externalIf}".useDHCP = true;
  time.timeZone = "UTC";

  services.smartd.enable = lib.mkForce false;
}
