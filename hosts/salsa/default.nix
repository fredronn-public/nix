{ pkgs, lib, ... }:{
    imports = [
        ./boot.nix
        ./hardware-configuration.nix
        ./filesystems.nix
        #./wireguard.nix
        #./mqtt.nix
        #./networklinks.nix
        #./postgres.nix
        #./influxdb.nix
        #./containers.nix
        #./openldap.nix
        ./restic.nix
        # ./../../users/fredrik.nix
    ];
    config = {

        systemd.services.systemd-networkd-wait-online.enable = lib.mkForce false;

        #boot.kernel.sysctl = {
        #    "net.ipv4.conf.all.send_redirects" = 0;
        #    "net.ipv4.conf.default.send_redirects" = 0;
        #};
        ronnvall.system.type = "server";
        ronnvall.graphical.enable = false;
        ronnvall.users.enable = true;
        ronnvall.bird = {
            enable = true;
            routerId = "10.200.101.68";
        };

        ronnvall.nftables = {
            enable = true;
        };

        ronnvall.wifi.enable = true;

        #ronnvall.duckdns.enable = true;
        #ronnvall.duckdns.domain = "salsaboob.duckdns.org";

        environment.systemPackages = with pkgs; [ screen ];

        #services.postfix.enable = true;
        #services.postfix.networks = [
        #    "10.200.101.0/24"
        #    "172.16.0.0/12"
        #    "192.168.0.0/16"
        #];

        #services.postfix.domain = "ronnvall.com";
        #services.postfix.hostname = "salsa.ronnvall.com";
        #services.syncoid = {
        #    interval = "daily";
        #    enable = true;
        #    commonArgs = [
        #        "--no-sync-snap"
        #    ];
        #    commands = {
        #        "salsa/userdata" = {
        #            recursive = true;
        #            source = "salsa/userdata";
        #            target = "seagate_expansion_8TB/backup/salsa/userdata";
        #        };
        #        "salsa/var" = {
        #            recursive = true;
        #            source = "salsa/var";
        #            target = "seagate_expansion_8TB/backup/salsa/var";
        #        };
        #    };
        #};
        services.openssh.hostKeys = [
          { type = "rsa"; bits = 4096; path = "/persist/ssh_host_rsa_key"; }
          { type = "ed25519"; path = "/persist/ssh_host_ed25519_key"; }
        ];

    };
}
