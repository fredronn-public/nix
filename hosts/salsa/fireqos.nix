{ pkgs, lib, ... }:
{
    config = {
        fireqos = {
            enable = true;
            config = ''
DEVICE=WAN
INPUT_SPEED=950Mbit
OUTPUT_SPEED=950Mbit

interface $DEVICE world-in input rate $INPUT_SPEED $LINKTYPE balanced
   class voip
      match udp port 5060
      match udp dports 10000:10100
      match sports 3478,5349

   class interactive
      match udp port 53
      match tcp port 22
      match icmp
      match tcp sports 5222,5228
      match tcp sports 5223

   class facetime
      match udp ports 3478:3497,16384:16387,16393:16402

   class vpns
      match tcp port 1723
      match gre
      match dport 1195:1198

   class surfing
      match tcp sports 0:1023

   class synacks                       # the new synacks class
      match tcp syn                    # TCP packets with SYN set
      match tcp ack                    # small TCP packets with ACK set

   class default                       # added the default class

   class torrents
      match dports 6881:6999           # official torrent ports
      match dport 51414 prio 1         # my torrent client
      match sports 16384:65535 dports 16384:65535 # my trick to match torrents

interface $DEVICE world-out output rate $OUTPUT_SPEED $LINKTYPE balanced
   class voip
      match udp port 5060
      match udp sports 10000:10100
      match dports 3478,5349

   class interactive
      match udp port 53
      match tcp port 22
      match icmp
      match tcp dports 5222,5228
      match tcp dports 5223

   class facetime
      match udp ports 3478:3497,16384:16387,16393:16402

   class vpns
      match tcp port 1723
      match gre
      match sport 1195:1198

   class surfing
      match tcp dports 0:1023

    class synacks                       # the new synacks class
       match tcp syn                    # TCP packets with SYN set
       match tcp ack                    # small TCP packets with ACK set

    class default                       # added the default class

   class torrents
       match sports 6881:6999           # official torrent ports
       match sport 51414 prio 1         # my torrent client
       match sports 16384:65535 dports 16384:65535 # my trick to match torrents
            '';
    };
}

