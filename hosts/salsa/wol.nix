{ pkgs, lib, ... }:
{
    config = {
        systemd.services.mqtt_wol = {
            serviceConfig.Type = "simple";
            serviceConfig.Restart = "always";
            script = ''
                HOST="127.0.0.1"
                TOPIC="wol/wake"
                COMMAND="${pkgs.mosquitto}/bin/mosquitto_sub"
                COMMAND="$COMMAND -h $HOST -t $TOPIC"
                $COMMAND | while read mac; do
                    ${pkgs.wol} 255.255.255.255 $mac
                done
            '';
        };
    };
}

