{ pkgs, config, lib, ... }:
with lib;
{
    config = {
        sops.secrets."wireguard/hosts/salsa/private".sopsFile = ./../../wireguard.yaml;
        sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;

        ronnvall.bird.ospfInterfaces = [ "wg-*" ];
        ronnvall.nftables.allowedInterfaces = [ "wg-*" ];
        ronnvall.nftables.allowedUDPPorts = mapAttrsToList (name: value: "${toString value.listenPort}") config.networking.wireguard.interfaces;
        networking.wireguard.interfaces = {
            "wg-burrito" = {
                listenPort = 51821;
                allowedIPsAsRoutes = false;
                ips = [ "10.200.101.4/31" ];
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "akC00NL5hxSTEwf/lKeRLV1zUc5AjpBabZ6of1EuyCM=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive=10;
                }];
            };
            "wg-nixberry" = {
                listenPort = 51823;
                allowedIPsAsRoutes = false;
                ips = [ "10.200.101.228/31" ];
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "2CXlae+ci8NX+NFrYIVzguB+wuiU2h1AmPSPzFkBGC4=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive=10;
                }];
            };
            "wg-apu1" = {
                listenPort = 51825;
                allowedIPsAsRoutes = false;
                ips = [ "10.200.101.232/31" ];
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "9nILribqXYtHLqeXMUR68/X6Zc6HAfUzFWLJy6MqtQw=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive = 10;
                }];
            };
            "wg-nixiso" = {
                listenPort = 51827;
                ips = [ "10.200.101.236/31" ];
                allowedIPsAsRoutes = false;
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "2BBPTbBOTiPKWAokuFbJQSl1cEKkCPEUV+1eA/VWXCo=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive = 10;
                }];
            };
            "wg-cloudberrypi" = {
                listenPort = 51829;
                ips = [ "10.200.101.240/31" ];
                allowedIPsAsRoutes = true;
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "HcEQ+5xoXxzmRy2ZeryrSvB1oThO6BPbKL34HDkBS2M=";
                    allowedIPs = ["10.200.101.241/32"];
                    persistentKeepalive = 10;
                }];
            };
            "wg-lengue" = {
                listenPort = 51830;
                ips = [ "10.200.101.242/31" ];
                allowedIPsAsRoutes = false;
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "mVllvUBbE/20MwonoH3nrwoyrg/9PhYsXj/k1eWCh24=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive = 10;
                }];
            };
            "wg-ampere" = {
                listenPort = 51831;
                ips = [ "10.200.101.244/31" ];
                allowedIPsAsRoutes = false;
                privateKeyFile = config.sops.secrets."wireguard/hosts/salsa/private".path;
                peers = [{
                    publicKey = "wnMqAGL3hgj9/KcqREgS8o+flR1DN/b3V2Kxl8juZSU=";
                    allowedIPs = ["0.0.0.0/0"];
                    persistentKeepalive = 10;
                }];
            };

        };
    };
}
