
{ pkgs, config, ... }:{
    config = {
        ronnvall.bird.ospfInterfaces = [ "lan" ];
        services.udev.extraRules = ''
        ATTR{address}=="00:e0:67:05:a9:f6", NAME="WAN"
        '';
        systemd.network = {
            links = {
                "WAN" = {
                    enable = true;
                    matchConfig = {
                        PermanentMACAddress="00:e0:67:05:a9:f6";
                    };
                    linkConfig = {
                        Name="WAN";
                        Description="WAN Interface";
                    };
                };
            };
            networks = {
                "WAN" = {
                    name="WAN";
                    enable = true;
                    matchConfig = { Name="WAN"; };
                    networkConfig = { DHCP="yes"; };
                    dhcpV6Config = { PrefixDelegationHint = "::/56"; };
                    dhcpConfig = { RouteMetric="10"; };
                };
                "switch" = {
                    enable = true;
                    matchConfig = { Name="switch"; };
                    networkConfig = { VLAN = [ "lan" "isolated" "iot" "mgmt" ]; ConfigureWithoutCarrier="yes"; };
                    extraConfig = ''
                        [BridgeVLAN]
                        VLAN=1
                        [BridgeVLAN]
                        VLAN=66
                        [BridgeVLAN]
                        VLAN=99
                        [BridgeVLAN]
                        VLAN=255
                    '';
                };
                "switchdevs" = {
                    enable = true;
                    matchConfig = { Name="en*"; };
                    networkConfig = { Bridge = "switch"; ConfigureWithoutCarrier="yes"; };
                    extraConfig = ''
                        [BridgeVLAN]
                        PVID=1
                        VLAN=1
                        EgressUntagged=1
                        [BridgeVLAN]
                        VLAN=66
                        [BridgeVLAN]
                        VLAN=99
                        [BridgeVLAN]
                        VLAN=255
                    '';
                };
                "lan" = {
                    name="lan";
                    enable = true;
                    matchConfig = { Name="lan"; };
                    networkConfig = { ConfigureWithoutCarrier="yes"; MulticastDNS="yes"; };
                    address = [ "172.16.1.1/24" ];
                    extraConfig = ''
                        [Network]
                        IPv6SendRA=yes
                        DHCPv6PrefixDelegation=yes
                    '';
                };
                "isolated" = {
                    name="isolated";
                    enable = true;
                    matchConfig = { Name="isolated"; };
                    networkConfig = { ConfigureWithoutCarrier="yes"; MulticastDNS="no"; };
                    address = [ "172.16.66.1/24" ];
                };
                "iot" = {
                    name="iot";
                    enable = true;
                    matchConfig = { Name="iot"; };
                    networkConfig = { ConfigureWithoutCarrier="yes"; MulticastDNS="no"; };
                    address = [ "172.16.99.1/24" ];
                };
                "mgmt" = {
                    name="mgmt";
                    enable = true;
                    matchConfig = { Name="mgmt"; };
                    networkConfig = { ConfigureWithoutCarrier="yes"; MulticastDNS="no"; };
                    address = [ "172.16.255.1/24" ];
                };
            };
            netdevs = {
                "lan" = {
                    enable = true;
                    netdevConfig = { Name = "lan"; Kind = "vlan"; };
                    vlanConfig = { Id = 1; };
                };
                "isolated" = {
                    enable = true;
                    netdevConfig = { Name = "isolated"; Kind = "vlan"; };
                    vlanConfig = { Id = 66; };
                };
                "iot" = {
                    enable = true;
                    netdevConfig = { Name = "iot"; Kind = "vlan"; };
                    vlanConfig = { Id = 99; };
                };
                "mgmt" = {
                    enable = true;
                    netdevConfig = { Name = "mgmt"; Kind = "vlan"; };
                    vlanConfig = { Id = 255; };
                };
                "switch" = {
                    enable = true;
                    netdevConfig = {
                        Name="switch";
                        Kind="bridge";
                    };
                    extraConfig = ''
                        [Bridge]
                        DefaultPVID=1
                        VLANFiltering=yes
                    '';
                };
            };
        };
    };
}


