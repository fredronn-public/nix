{ config, pkgs, lib, ... }:{
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only
  #boot.supportedFilesystems = [ "zfs" ];
  #boot.zfs.devNodes = "/dev/disk/by-id";
  #boot.zfs.requestEncryptionCredentials = false;
  #boot.zfs.extraPools = [ "seagate_expansion_8TB" ];


  networking.hostName = "salsa"; # Define your hostname.
  networking.hostId = "7c2f2ec9";

  # Set your time zone.
  time.timeZone = "UTC";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  #networking.useDHCP = false;

  #boot.initrd.postDeviceCommands = lib.mkAfter ''
  #  zfs rollback -r salsa/ROOT/rootfs@empty
  #'';

}
