{ config, ... }:
{

  sops.secrets."wireguard/hosts/paddington/private".sopsFile = ./../../wireguard.yaml;
  sops.secrets."wireguard/hosts/paddington/private_c3se".sopsFile = ./../../wireguard.yaml;
  sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
  networking.wireguard.interfaces = {
    "wg-salsa" = {
      ips = [ "10.200.101.239/31" ];
      allowedIPsAsRoutes = false;
      privateKeyFile = config.sops.secrets."wireguard/hosts/paddington/private".path;
      peers = [{
          publicKey = "lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
          allowedIPs = [ "0.0.0.0/0" ];
          endpoint = "apu.ronnvall.com:51828";
      }];
    };
    "wg-c3se" = {
        ips = [ "192.168.211.12/32" ];
        allowedIPsAsRoutes = true;
        privateKeyFile = config.sops.secrets."wireguard/hosts/paddington/private_c3se".path;
        peers = [{
          publicKey = "LQXSE5Bz8gu69SSURaqzb95gAltxLWOXhQ+VauUIQA0=";
          allowedIPs = [
              "192.168.211.13/32"
              "192.168.211.201/32"
              "129.16.125.128/27"
              "129.16.125.160/28"
              "129.16.125.176/29"
              "129.16.125.184/32"
              "129.16.125.186/31"
              "129.16.125.188/30"
              "129.16.125.192/26"
              "129.16.125.154/32"
              "10.0.0.10/32"
              "10.0.1.0/24"
              "10.0.2.0/24"
              "10.0.0.0/8"
          ];
          endpoint = "129.16.125.185:51820";
        }];
    };
  };
}
