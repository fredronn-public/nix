{
    config = {
        systemd.nspawn = {
            "arch" = {
                enable = true;
                networkConfig = {
                    Bridge="br-container";
                };
            };
        };
    };
}
