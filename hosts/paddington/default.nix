{ lib, config, pkgs, ... }:{
  imports = [
    ./../../users/fredronn.nix
    ./hardware-configuration.nix
    ./wireguard.nix
    ./nspawn.nix
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  services.containerbridge.enable = true;
  services.containerbridge.networkAddress = "172.20.2.1/24";

  #services.xserver.displayManager.gdm.wayland = lib.mkForce true;

  ronnvall.autoUpdate.enable = true;
  ronnvall.autoUpdate.hostName = "paddington";
  ronnvall.graphical.enable = true;
  ronnvall.zfs.enable = true;
  ronnvall.zfs.persist = true;
  ronnvall.zfs.zvol = "/dev/zroot/var/lib/docker";
  ronnvall.wifi.enable = true;

  system.autoUpgrade.allowReboot = lib.mkForce false;

  ronnvall.users.users = { 
    fredronn = {
      isNormalUser = true;
      extraGroups = [ "wheel" "docker" "audio" "libvirtd" "video" ];
      hashedPassword = "$6$U358jpoNwKMdIVAk$KGSInRmLRLCHwdXcdWGMjZYBOUKBffgDmKBa3pLr1E6h3Colmg29G6g.s362/NVMNHK09QcpiMarYZaE75cp00";
    };
  };
  ronnvall.users.managed = [ "fredronn" ];

  ronnvall.bird = { enable=true; routerId="10.200.101.72"; };
  time.timeZone = "Europe/Stockholm";
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  networking.hostName = "paddington";
  networking.hostId = "1183d283";
  #systemd.services.systemd-networkd-wait-online.enable = false;
  #systemd.services.NetworkManager-wait-online.enable = false;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
