{config, pkgs, lib, ...}:
let
  ageFile = pkgs.writeText "agekey.txt" (builtins.readFile ~/.config/sops/age/keys.txt);
  disko = (builtins.getFlake "github:nix-community/disko").packages.x86_64-linux.default;
in
{
  imports = [
  ];

  sops.secrets."wireguard/hosts/iso/private".sopsFile = ./../../wireguard.yaml;
  sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
  
  networking.wireguard.interfaces = {
    "wg-salsa" = {
        allowedIPsAsRoutes = false;
        ips = [ "10.200.101.237/31" ];
        privateKeyFile = config.sops.secrets."wireguard/hosts/iso/private".path;
        peers = [{
            publicKey="lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
            allowedIPs=["0.0.0.0/0"];
            endpoint="apu.ronnvall.com:51827";
            persistentKeepalive=10;
        }];
    };
  };

  boot.zfs.enableUnstable = true;
  networking.hostId = "364f88c8";
  boot.supportedFilesystems = [ "zfs" ];
  ronnvall.wifi.enable = true;
  ronnvall.wifi.fiveg = false;
  ronnvall.bird.enable = true;
  ronnvall.bird.routerId = "10.200.101.237";
  ronnvall.bird.ospfInterfaces = [ "wg-salsa" ];
  ronnvall.nftables.enable = true;
  ronnvall.nftables.allowedInterfaces = [ "wg-salsa" ];
  ronnvall.hardware.rtw89.enable = true;
  
  systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];

  sops.age.keyFile = "${ageFile}";
  sops.age.sshKeyPaths = lib.mkForce [];
  sops.gnupg.sshKeyPaths = lib.mkForce [];

  environment.systemPackages = with pkgs; [
    minicom
    screen
    disko
  ];
}
