{ disks ? [ "/dev/disk/by-path/pci-0000:00:11.0-ata-1" ], ... }: {
  disko.devices = {
    disk = {
      x = {
        type = "disk";
        device = builtins.elemAt disks 0;
        content = {
          type = "table";
          format = "msdos";
          partitions = [
            {
              name = "boot";
              start = "2MiB";
              end = "512MiB";
              bootable = true;
              content = {
                type = "filesystem";
                format = "ext4";
                mountpoint = "/boot";
              };
            }
            {
              name = "root";
              start = "512MiB";
              end = "100%";
              content = {
                type = "btrfs";
                extraArgs = [ "-f" ]; # Override existing partition
                subvolumes = {
                  "/rootfs" = {
                    mountpoint = "/";
                    mountOptions = [ "compress=lzo" ];
                  };
                  "/home" = {
                    mountOptions = [ ];
                  };
                  "/nix" = {
                    mountOptions = [ "compress=lzo" "noatime" ];
                  };
                  "/persist" = {
                    mountOptions = [ ];
                  };
                };
              };
            }
          ];
        };
      };
    };
  };
}
