{ lib, pkgs, ... }:{
    imports = [
        ./hardware.nix
        ./software.nix
        ./filesystems.nix
    ];
    config = {
        ronnvall.system.type = "server";
        ronnvall.graphical.enable = false;
        ronnvall.autoUpdate.enable = true;
        ronnvall.users.enable = true;
        ronnvall.wifi.enable = false;
        ronnvall.wifi.fiveg = false;
        services.openssh.hostKeys = [
          { type = "rsa"; bits = 4096; path = "/persist/ssh_host_rsa_key"; }
          { type = "ed25519"; path = "/persist/ssh_host_ed25519_key"; }
        ];
        environment.systemPackages = with pkgs; [ screen compsize ];
        sops.age.sshKeyPaths = [ "/persist/ssh_host_ed25519_key" ];
        ronnvall.restic = {
            enable = true;
            backupPaths = [ "/home" "/var/backup" "/srv" "/persist" ];
            backupTargets = [ "jotta" "burrito" "apu2" ];
        };

    };
}
