{ lib, pkgs, ... }:with lib;
{
    fileSystems = {
      "/boot" = { device = "/dev/sda1"; fsType = "ext4"; options = [ "nofail" ]; };
      "/" = { device = "none"; fsType = "tmpfs"; options = [ "defaults" "size=128M" "mode=755" ]; };
      #"/" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=rootfs" "compress=zstd" ]; };
      "/nix" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=nix" "noatime" "compress=zstd" ]; };
      "/home" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=home" "compress=zstd" ]; };
      "/persist" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=persist" "compress=zstd" ]; neededForBoot = true; };
      "/var" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=var" "compress=zstd" ]; neededForBoot = true; };
      "/var/cache" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=var-cache" "compress=zstd" ]; neededForBoot = false; };
      "/var/lib/rancher" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=var-lib-rancher" "compress=zstd" ]; };
      "/var/lib/containerd" = { device = "/dev/sda2"; fsType = "btrfs"; options = [ "subvol=var-lib-containerd" "compress=zstd" ]; };
    };
}
