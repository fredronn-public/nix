{ config, lib, ... }:with lib;
{
    boot.initrd.availableKernelModules = [ "ahci" "xhci_hcd" "xhci_pci" "xhci-pci-renesas" "usb_storage" ];
    boot.initrd.kernelModules = [ ];
    boot.kernelModules = [ "kvm-amd" ];
    boot.kernelParams = [ "amd_iommu=off" "usbcore.autosuspend=-1" ];
    boot.loader.grub = {
        enable = true;
        device = "/dev/sda";
        extraConfig = "serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1; terminal_input serial; terminal_output serial";
    };
    powerManagement.cpuFreqGovernor = mkDefault "performance";
    hardware.enableRedistributableFirmware = mkDefault true;
    hardware.cpu.amd.updateMicrocode = mkDefault config.hardware.enableRedistributableFirmware;
}
