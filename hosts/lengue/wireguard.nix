{ config, pkgs, ... }:{
    sops.secrets."wireguard/hosts/lengue/private".sopsFile = ./../../wireguard.yaml;
    sops.secrets."wireguard/pre_shared_key".sopsFile = ./../../wireguard.yaml;
    ronnvall.bird.ospfInterfaces = [ "wg-salsa" ];
    networking.wireguard.interfaces = {
        "wg-salsa" = {
            ips = [ "10.200.101.243/31" ];
            allowedIPsAsRoutes = false;
            privateKeyFile = config.sops.secrets."wireguard/hosts/lengue/private".path;
            peers = [{
                publicKey = "lQtWIDmJc3ihZHb9ruquK7dkSdWrA4YBIxBIhv9b1xg=";
                allowedIPs = [ "0.0.0.0/0" ];
                endpoint = "62.63.229.149:51830";
                persistentKeepalive=10;
            }];
        };
    };
    systemd.services.check_wg = {
        serviceConfig.Type = "oneshot";
        script = ''
            WG=${pkgs.wireguard-tools}/bin/wg
            AWK=${pkgs.gawk}/bin/awk
            GREP=${pkgs.gnugrep}/bin/grep
            SYSTEMCTL=${pkgs.systemd}/bin/systemctl
            ENDPOINT=$($WG show wg-salsa | $GREP 'endpoint:' | $AWK '{print $2}')
            if [ "$ENDPOINT" != "62.63.229.149:51830" ]; then
                echo $ENDPOINT is wrong...
                $SYSTEMCTL restart wireguard-wg-salsa.service
            fi
        '';
    };
    
    systemd.timers.check_wg = {
        wantedBy = [ "timers.target" ];
        partOf = [ "check_wg.service" ];
        timerConfig.OnCalendar = "*-*-* *:*:00";
    };
 
}
