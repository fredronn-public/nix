{ stdenv, lib, fetchFromGitHub, pkgs, kernel }:

let
  modDestDir = "$out/lib/modules/${kernel.modDirVersion}/kernel/drivers/net/wireless/realtek/rtw89";
in
stdenv.mkDerivation {
  pname = "rtw8852be";
  version = "2022-10-16";

  src = fetchFromGitHub {
    owner = "lwfinger";
    repo = "rtw89";
    rev = "main";
    sha256 = "19ApYiEvA0E6qgf5XQc03paZ+ghjZL8JoC3vSYYw3xU=";
    #rev = "8046f59a7eb4a311fcdd7e6f4a0ff81a353bc1c4";
    #sha256 = "evpJ47hOCJ7s22Ap3o+tZ7gFlQCWH6ta9Ubz7TCl2wU=";
  };

  hardeningDisable = [ "pic" "format" ];
  nativeBuildInputs = kernel.moduleBuildDependencies;
  buildInputs = [
    pkgs.bc
    pkgs.openssl
    pkgs.mokutil
  ];
  #  makeFlags = kernel.makeFlags ++ [ "CFLAGS=-Wno-error" "KSRC=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build" ];
  makeFlags = kernel.makeFlags ++ [
    "KSRC=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"
    "KERNELRELEASE=${kernel.modDirVersion}"                                 # 3
    "KERNEL_DIR=${kernel.dev}/lib/modules/${kernel.modDirVersion}/build"    # 4
    "INSTALL_MOD_PATH=$(out)"                                               # 5
  ];

  enableParallelBuilding = true;

  buildPhase = ''
    make
  '';

  installPhase = ''
    runHook preInstall

    mkdir -p ${modDestDir}
    find . -name '*.ko' -exec cp --parents {} ${modDestDir} \;
    find ${modDestDir} -name '*.ko' -exec xz -f {} \;

    runHook postInstall
  '';

  meta = with lib; {
    description = " Driver for Realtek rtw8852be, an 802.11ax device";
    homepage = "https://github.com/lwfinger/";
    license = with licenses; [ gpl2Only ];
#    maintainers = with maintainers; [ tvorog ];
    platforms = platforms.linux;
    broken = kernel.kernelOlder "5.15";
    priority = -1;
  };
}
