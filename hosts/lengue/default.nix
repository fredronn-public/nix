# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./wireguard.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.supportedFilesystems = [ "zfs" ];
  networking.hostId = "364f88c7";
  networking.hostName = "lengue";

  ronnvall.hardware.rtw89.enable = true;
  #ronnvall.chalmers.enable = true;
  ronnvall.graphical.enable = true;
  ronnvall.graphical.variant = "i3";
  ronnvall.zfs.enable = true;
  ronnvall.zfs.persist = true;
  ronnvall.wifi.enable = true;
  ronnvall.restic.backupTargets = [ "burrito" "salsa" ];
  ronnvall.bird = { enable=true; routerId="10.200.101.74"; };
  ronnvall.system.type = "laptop";
  ronnvall.wifi.fiveg = true;

  # Set your time zone.
  time.timeZone = "Europe/Stockholm";

  services.tlp = {
    enable = if config.ronnvall.graphical.variant == "gnome" then false else true;
    settings = {
      STOP_CHARGE_THRESH_BAT0="1";
      CPU_SCALING_GOVERNOR_ON_BAT="ondemand";
      CPU_SCALING_GOVERNOR_ON_AC="performance";
    };
  };

  virtualisation.libvirtd.enable = true;

  virtualisation.podman.enable = true;
  #virtualisation.podman.defaultNetwork.dnsname.enable = true;
  virtualisation.podman.defaultNetwork.settings.dns_enabled = true;

  virtualisation.podman.extraPackages = with pkgs; [ zfs podman-compose dnsname-cni ];
  environment.systemPackages = with pkgs; [ podman-compose qemu virt-manager ];

  fileSystems."/var/lib/containers" = { device = "/dev/zroot/var/lib/containers"; fsType = "xfs"; };
}

