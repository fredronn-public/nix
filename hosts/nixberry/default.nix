{ config, lib, pkgs, ... }: {
  imports = [
    ./wireguard.nix
  ];
  #boot.loader.raspberryPi.enable = lib.mkAfter true;
  #boot.loader.raspberryPi.version = 3;
  #boot.loader.raspberryPi.uboot.enable = true;

  ## NixOS wants to enable GRUB by default
  #boot.loader.grub.enable = false;
  ## Enables the generation of /boot/extlinux/extlinux.conf
  ##boot.loader.generic-extlinux-compatible.enable = true;

  ##boot.kernelPackages = lib.mkForce pkgs.linuxPackages_latest;

  #fileSystems = {
  #  "/" = {
  #    device = "/dev/disk/by-label/NIXOS_SD";
  #    fsType = "ext4";
  #  };
  #};

  #nixpkgs.localSystem = {
  #  system = "aarch64-linux";
  #  config = "aarch64-unknown-linux-gnu";
  #};

  hardware.enableRedistributableFirmware = lib.mkForce false;
  hardware.firmware = with pkgs; [ 
    raspberrypiWirelessFirmware
    rtw88-firmware
  ];
  ronnvall.k3s.enable = true;
  ronnvall.autoUpdate.enable = lib.mkForce false;
  ronnvall.telegraf.enable = lib.mkForce false;
  ronnvall.users.enable = true;
  ronnvall.wifi.enable = true;
  ronnvall.wifi.fiveg = false;
  ronnvall.nftables.enable = true;
  ronnvall.nftables.natPorts = [ "eth0" ];
  ronnvall.bird.enable = true;
  ronnvall.bird.routerId = "10.200.101.71";
  systemd.services.sshd.wantedBy = pkgs.lib.mkForce [ "multi-user.target" ];
  networking.hostName = "nixberry";
}
