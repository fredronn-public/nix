{ pkgs, ... }:
{
    systemd.services.check_zigbee2mqtt = {
        serviceConfig.Type = "oneshot";
        script = ''
            #!${pkgs.bash}/bin/bash
            HOST="172.16.1.1"
            TOPIC="zigbee/bridge/state"
            COMMAND="${pkgs.mosquitto}/bin/mosquitto_sub"
            COMMAND="$COMMAND -h $HOST -t $TOPIC -C 1 -W 1"
            STATE=$($COMMAND)
            if [ "$STATE" = "offline" ]; then
                echo "Restart zigbee2mqtt due to bridge marked offline"
                ${pkgs.docker}/bin/docker restart zigbee2mqtt
            fi
        '';
    };
    systemd.timers.check_zigbee2mqtt = {
        wantedBy = [ "timers.target" ];
        partOf = [ "check_zigbee2mqtt.service" ];
        timerConfig.OnCalendar = "*-*-* *:*:00";
    };
}

