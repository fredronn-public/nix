{ pkgs, lib, ... }:with lib; with pkgs; {
  imports = [
    ./../common/software/packages.nix
    ./../common/users/fish.nix
  ];
  config = {
    security.sudo = { enable = true; wheelNeedsPassword = false; };
    networking = { enableIPv6 = false; useDHCP = mkForce true; firewall.enable = false; };
    users.users.user = { isNormalUser = true; extraGroups = [ "wheel" ]; };
    documentation = { nixos.enable = false; man.enable = false; };
    services.journald.extraConfig = ''
    SystemMaxUse=10M
    Storage=volatile
    '';
    networking.useHostResolvConf = mkForce false;
    networking.resolvconf.useLocalResolver = mkForce true;
    services.unbound = {
      enable = true;
      settings.remote-control.control-enable = true;
    };
    system.stateVersion = "23.05";
  };
}