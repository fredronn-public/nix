{ pkgs, lib, ... }: with pkgs;
let
  wgnord = callPackage ./../../common/software/wgnord.nix {};
  fix-deluge-permissions = writeShellApplication {
    name = "fix-deluge-permissions";
    
    runtimeInputs = [ coreutils-full ];
    
    text = ''
      chmod a+rwx /media/downloads
    '';
  };
in
{
  config = {
    containers.deluge = {
      ephemeral = true;
      autoStart = true;
      privateNetwork = true;
      forwardPorts = [ {
        containerPort = 8112;
        hostPort = 8112;
        protocol = "tcp";
      } ];
      hostBridge = "br-container";
      bindMounts = {
        wgnord = {
          hostPath = "/var/lib/wgnord/deluge";
          mountPoint = "/var/lib/wgnord";
          isReadOnly = false;
        };
        deluge = {
          hostPath = "/var/lib/deluge";
          mountPoint = "/srv/deluge";
          isReadOnly = false;
        };
        tv = {
          hostPath = "/media/tv";
          mountPoint = "/media/tv";
          isReadOnly = false;
        };
        film = {
          hostPath = "/media/film";
          mountPoint = "/media/film";
          isReadOnly = false;
        };
        downloads = {
          hostPath = "/media/downloads";
          mountPoint = "/media/downloads";
          isReadOnly = false;
        };
      };
      config = { pkgs, lib, config, ... }: with lib; {
        imports = [
          ./../default.nix
        ];
        config = {
          environment.systemPackages = with pkgs; [ wireguard-tools ];
          networking.interfaces.eth0.ipv4.routes = [
            { address = "192.108.210.172";
              prefixLength = 32;
              via = "172.18.2.1";
              options = { metric = "10"; }; }
            { address = "172.16.0.0";
              prefixLength = 12;
              via = "172.18.2.1";
              options = { metric = "10"; }; }
            { address = "10.0.0.0";
              prefixLength = 8;
              via = "172.18.2.1";
              options = { metric = "10"; }; }
          ];
          systemd.services.network-addresses-eth0 = { after = [ "dhcpcd.service" ]; };
          systemd.services.wgnord = {
            enable = true;
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" "dhcpd.service" ];
            serviceConfig = {
              Type = "notify";
              Restart = "always";
              RestartSec = 10;
              TimeoutSec = 10;
              WatchdogSec = 10;
            };
            script = ''
              #!${pkgs.bash}/bin/bash
              ${wgnord}/bin/wgnord c se
              sleep 5;
              ${systemd}/bin/systemd-notify --ready
              while(true); do
                FAIL=0;
                CANHAZ=$(${curl}/bin/curl -s canhazip.com)
                DIG=$(${dnsutils}/bin/dig +short burritoboob.duckdns.org)
                if [ "$DIG" == "$CANHAZ" ]; then FAIL=1; fi
                if [[ $FAIL -eq 0 ]]; then
                  ${systemd}/bin/systemd-notify WATCHDOG=1
                else
                  echo "canhaz: $CANHAZ, dig: $DIG"
                  ${wgnord}/bin/wgnord d
                fi
                sleep 5
              done
            '';
          };
          systemd.services.deluged.after = [ "wgnord.service" ];
          systemd.services.deluged.serviceConfig.ExecStartPost = [
            "${fix-deluge-permissions}/bin/fix-deluge-permissions"
          ];


          services.deluge = {
            enable = true;
            web.enable = true;
            authFile = pkgs.writeText "deluge-auth" ''
            localclient:deluge:10
            deluge:deluge:10
            ''; 
            declarative = true;
            dataDir = "/srv/deluge";
            config = {
              allow_remote = true;
              download_location = "/media/downloads/";
              max_upload_speed = "1000.0";
              share_ratio_limit = "2.0";
              enabled_plugins = [
                "Label"
                "Execute"
                "Notifications"
              ];
            };
          };
        };
      };
    };
  };
}
