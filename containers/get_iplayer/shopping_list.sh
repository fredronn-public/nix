
LOCKFILE=./shopping_list.lock
LOCK_TIMEOUT=5
DIR=/media/tv/iplayer
pushd $DIR

touch $LOCKFILE
exec {FD}<>$LOCKFILE
if flock -x -w $LOCK_TIMEOUT $FD; then
wgnord c uk
unbound-control flush_zone .
sleep 5s
DATA=$(curl -s -X GET -H "Authorization: Bearer $HASS_TOKEN" "$HASS_URL/api/shopping_list")
echo $DATA | jq '.[] | select(.complete == false)' | jq -r .name | while read url; do
    if curl --head --silent --fail "$url" 2> /dev/null; then
    echo valid $url
    if get_iplayer --atomicparsley ${atomicparsley}/bin/atomicparsley "$url"; then
#get_iplayer --metadata "$url"
        curl -s -X POST -H "Authorization: Bearer $HASS_TOKEN" "$HASS_URL/api/services/shopping_list/complete_item" -d "{\"name\": \"$url\"}"
    fi
    else
    echo invalid $url
    curl -s -X POST -H "Authorization: Bearer $HASS_TOKEN" "$HASS_URL/api/services/shopping_list/complete_item" -d "{\"name\": \"$url\"}"
    fi
done
wgnord d
curl -s -X POST -H "Authorization: Bearer $HASS_TOKEN" "$HASS_URL/api/services/shopping_list/clear_completed_items"
pymedia /media/tv/iplayer /media/tv
else
echo "failed to obtain lock!"
fi
popd
