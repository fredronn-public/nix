{ pkgs, lib, ... }: with pkgs;
let
  wgnord = callPackage ./../../common/software/wgnord.nix {};
  pymedia = callPackage ./../../pkgs/pymedia/derivation.nix {};
  shopping_list = pkgs.resholve.writeScriptBin "shopping_list.sh" {
    inputs = [ curl get_iplayer atomicparsley coreutils util-linux wgnord jq which pymedia unbound ];
    interpreter = "${bash}/bin/bash";
    execer = [
      "cannot:${pymedia}/bin/pymedia"
      "cannot:${get_iplayer}/bin/get_iplayer"
      "cannot:${util-linux}/bin/flock"
      "cannot:${wgnord}/bin/wgnord"
      "cannot:${unbound}/bin/unbound-control"
    ];
  } (builtins.readFile ./shopping_list.sh);
in
{
  config = {
    environment.systemPackages = [ shopping_list pymedia ];
    systemd.services.shopping_list = {
      serviceConfig = { User = "root"; Type = "oneshot"; ExecStart = "${shopping_list}/bin/shopping_list.sh"; };
    };
    systemd.timers.shopping_list = {
      wantedBy = [ "timers.target" ];
      timerConfig = {
        OnBootSec = "5m";
        OnUnitActiveSec = "5m";
        Unit = "shopping_list.service";
      };
    };
  };
}