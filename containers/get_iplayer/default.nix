{ pkgs, lib, ... }: with pkgs; {
  config = {
    containers.get-iplayer = {
      ephemeral = true;
      autoStart = true;
      privateNetwork = true;
      hostBridge = "br-container";
      bindMounts = {
        wgnord = {
          hostPath = "/var/lib/wgnord/iplayer";
          mountPoint = "/var/lib/wgnord";
          isReadOnly = false;
        };
        tv = {
          hostPath = "/media/tv";
          mountPoint = "/media/tv";
          isReadOnly = false;
        };
      };
      config = { pkgs, lib, config, ... }: with lib; {
        imports = [
          ./../default.nix
          ./shopping_list.nix
        ];
        config = {
          services.unbound.settings.forward-zone = [{
            name = ".";
            forward-addr = [ "103.86.99.100" "103.86.96.100" ];
          }];
        };
      };
    };
  };
}