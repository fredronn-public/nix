
{ pkgs, config, ... }:{
    config = {
        systemd.services.mosquitto.serviceConfig = {
            RestartSec = 60;
        };
        services.mosquitto = {
            enable = true;
            listeners = [
                {
                    address = "172.16.1.1";
                    omitPasswordAuth = true;
                    settings = {
                        allow_anonymous = true;
                    };
                    acl = [
                        "topic readwrite #"
                    ];
                    users = {
                        shelly = {
                            password = "shelly";
                            acl = [
                                "readwrite #"
                            ];
                        };
                        tasmota = {
                            password = "tasmota";
                            acl = [
                                "readwrite #"
                            ];
                        };
                    };
                }
                {
                    address = "62.63.229.149";
                    settings = {
                        allow_anonymous = false;
                    };
                    users = {
                        owntracks = {
                            password = "tracksowned";
                            acl = [
                                "readwrite owntracks/#"
                            ];
                        };
                        shelly = {
                            hashedPassword = "$6$175b7xp7jtO6oN/l$fcqx5Vy3uosJ5r2b2kSZKA32F+nx9aRH7TgKGxx0Tr1Ni1HQS5SxKbwkxmQAYm7VDy+Ayy/hmX8IsOZn4XA15A==";
                            acl = [
                                "readwrite #"
                            ];
                        };
                        poweroff = {
                            hashedPassword = "$6$FK9Xtihzq6JHZZe0$StuD2IeAmxJ1j4+v6gP3rGFPq9/O6DqSTuKQ7WBfWTyCgKKzGAR+X5LPA3aj19bdYtj235FKSA4sSKeXV4X0BQ==";
                            acl = [
                                "readwrite #"
                            ];
                        };
                        telldusmq = {
                            hashedPassword = "$6$C8QTjuvvc1k5r38Q$1oIZ01wuXTOuimJLGQRI/NxfawRe6hVbuQnYdzZDH/DcITXdTVkEaY399PY5iYLJmZ/1CZZ6jgF77YKUMgV4Og==";
                            acl = [
                                "readwrite #"
                            ];
                        };
                        fredrik = {
                            hashedPassword = "$7$101$SECF4cbownkvQW7x$RgndpUTGaypkKLEx6cz5NDRyqqRjR9eeu1DHujL0Xyl7Y3j70vYKjInGS7i4uuYpu1TwacGfsDWfw5kG6Fi8oQ==";
                            acl = [
                                "readwrite #"
                            ];
                        };
                        server = {
                            acl = [
                                "readwrite #"
                            ];
                            password = "us32DaxCjFt1SkyaQwI7A0YKBhP2MxOD";
                        };
                    };
                }
            ];
        };
    };
}


