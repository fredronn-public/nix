{ config, pkgs, ... }:{
  services.postgresql = {
      enable = true;
      package = pkgs.postgresql_15;
      enableTCPIP = true;
      authentication = "host all all all md5";
      settings = {
          shared_buffers = "512MB";
          #Set when performing major version update
          #default_transaction_read_only = true;
      };
  };
  
  services.postgresqlBackup.enable = true;
  services.postgresqlBackup.compression = "zstd";
  ronnvall.restic.backupPaths = [ "/var/backup/postgresql" ];
  environment.systemPackages = [
    (pkgs.writeScriptBin "upgrade-pg-cluster" ''
        set -eux
        export LC_ALL="en_US.UTF-8"
        OLD_VERSION="14"
        NEW_VERSION="15"
        # XXX it's perhaps advisable to stop all services that depend on postgresql
        systemctl stop postgresql

        # XXX replace `<new version>` with the psqlSchema here
        export NEWDATA="/var/lib/postgresql/$NEW_VERSION"

        # XXX specify the postgresql package you'd like to upgrade to
        export NEWBIN="${pkgs.postgresql_15}/bin"

        export OLDDATA="${config.services.postgresql.dataDir}"
        export OLDBIN="${config.services.postgresql.package}/bin"

        install -d -m 0700 -o postgres -g postgres "$NEWDATA"
        cd "$NEWDATA"
        sudo -u postgres $NEWBIN/initdb -D "$NEWDATA"

        sudo -u postgres $NEWBIN/pg_upgrade \
          --old-datadir "$OLDDATA" --new-datadir "$NEWDATA" \
          --old-bindir $OLDBIN --new-bindir $NEWBIN \
          "$@"
    '')
  ];
}
