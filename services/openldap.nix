{ config, pkgs, ... }:{
    config = {
        sops.secrets."openldaprootpw" = { owner = config.services.openldap.user; };
        ronnvall.restic.backupPaths = [ "/var/lib/openldap" ];
        services.openldap = {
            enable = true;
            settings = {
              attrs.olcLogLevel = [ "stats" ];
              children = {
                "cn=schema".includes = [
                   "${pkgs.openldap}/etc/schema/core.ldif"
                   "${pkgs.openldap}/etc/schema/cosine.ldif"
                   "${pkgs.openldap}/etc/schema/inetorgperson.ldif"
                   "${pkgs.openldap}/etc/schema/nis.ldif"
                ];
                "olcDatabase={-1}frontend" = {
                  attrs = {
                    objectClass = "olcDatabaseConfig";
                    olcDatabase = "{-1}frontend";
                    olcAccess = [ "{0}to * by dn.exact=uidNumber=0+gidNumber=0,cn=peercred,cn=external,cn=auth manage stop by * none stop" ];
                  };
                };
                "olcDatabase={0}config" = {
                  attrs = {
                    objectClass = "olcDatabaseConfig";
                    olcDatabase = "{0}config";
                    olcAccess = [ "{0}to * by * none break" ];
                  };
                };
                "olcDatabase={1}mdb" = {
                  attrs = {
                    objectClass = [ "olcDatabaseConfig" "olcMdbConfig" ];
                    olcDatabase = "{1}mdb";
                    olcDbDirectory = "/var/lib/openldap/data";
                    olcDbIndex = [
                      "objectClass eq"
                      "cn pres,eq"
                      "uid pres,eq"
                      "sn pres,eq,subany"
                    ];
                    olcSuffix = "dc=ronnvall,dc=com";
                    olcRootDN = "cn=root,dc=ronnvall,dc=com";
                    olcRootPW.path = config.sops.secrets."openldaprootpw".path;
                    olcAccess = [
                        ''{0}to attrs=userPassword
                            by self write
                            by dn.exact="uid=authelia,ou=sysacct,dc=ronnvall,dc=com" write
                            by dn.exact="cn=root,dc=ronnvall,dc=com" write
                            by anonymous auth
                            by * none
                        ''
                        ''{1}to dn.base="" by * read''
                        ''{2}to dn.base="cn=Subschema" by * read''
                        ''{3}to *
                            by dn.exact="cn=root,dc=ronnvall,dc=com" write
                            by self write
                            by * read
                        ''
                    ];
                  };
                };
              };
            };
        };
    };
}
