{ config, pkgs, ... }:{
    services.influxdb.enable = true;
    services.influxdb.dataDir = "/var/lib/influxdb";
    #ronnvall.restic.backupPaths = [ "/var/lib/influxdb" ];
    systemd.services.influxdb.serviceConfig.TimeoutSec = 300;
    environment.systemPackages = with pkgs; [ influxdb ];
}
