{
  description = "Flake to manage my hosts";
  
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";
    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager/release-23.05";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    flake-utils.url   = "github:numtide/flake-utils";
  };
  
  outputs = { self, nixpkgs, home-manager, sops-nix, ... }@attrs:
  let
    installHostConfig = { config, lib, pkgs, ... }:
    let
      #ageFile = pkgs.writeText "agekey.txt" (builtins.readFile ~/.config/sops/age/keys.txt);
    in {
      #sops.age.keyFile = "${ageFile}";
      #sops.age.sshKeyPaths = lib.mkForce [];
      #sops.gnupg.sshKeyPaths = lib.mkForce [];
    };
    hostSharedConfig = { config, lib, pkgs, ... }: {
      imports = [
        home-manager.nixosModules.home-manager
        sops-nix.nixosModules.sops
      ];
      nixpkgs.config.allowUnfree = true;
      sops.defaultSopsFile = ./secrets.yaml;
    };
    userSharedConfig = { pkgs, config, ... }: {
        imports = [ sops-nix.homeManagerModule ./users/default.nix ./users/graphical.nix ];
        home.stateVersion = "23.05";
        programs.git.enable = true;
        programs.home-manager.enable = true;
    };
    mkUser = u: {
        imports = [ ./users/${u}.nix ];
        home = { username = u; homeDirectory = "/home/${u}"; };
        sops = {
            age.keyFile = "/home/${u}/.config/sops/age/keys.txt";
        };
    };
    mkHost = h: system: nixpkgs.lib.nixosSystem {
        specialArgs = attrs;
        system = system;
        modules = [ hostSharedConfig ./common (./. + "/hosts/${h}") ];
    };
    mkInstallHost = h: system: nixpkgs.lib.nixosSystem {
        specialArgs = attrs;
        system = system;
        modules = [ installHostConfig hostSharedConfig ./common (./. + "/hosts/${h}") ];
    };
    #mkHostWithUser = h: u: system: nixpkgs.lib.nixosSystem {
    #    specialArgs = attrs;
    #    system = system;
    #    modules = [ hostSharedConfig ./common (./. + "/hosts/${h}") {
    #        home-manager.users."${u}" = mkUser u;
    #    }];
    #};
    hosts = ["lengue" "ampere" "nuc" "burrito" "apu1" "apu2" "salsa" "chalupa"];
  in {
    #nixosConfigurations = builtins.listToAttrs (builtins.map(h:{
    #  name = h;
    #  value = (mkHost "${h}" "x86_64-linux");
    #}) hosts );
    nixosConfigurations = {
      ampere = (mkHost "ampere" "aarm64-linux");
      apu1 = (mkHost "apu1" "x86_64-linux");
      apu1_install = (mkInstallHost "apu1" "x86_64-linux");
      apu2 = (mkHost "apu2" "x86_64-linux");
      burrito = (mkHost "burrito" "x86_64-linux");
      burrito_install = (mkInstallHost "burrito" "x86_64-linux");
      chalupa = (mkHost "chalupa" "x86_64-linux");
      iso = (mkHost "iso" "x86_64-linux");
      lengue = (mkHost "lengue" "x86-64-linux");
      nixberry = (mkHost "nixberry" "aarch64-linux");
      nuc = (mkHost "nuc" "x86_64-linux");
      nuc_install = (mkInstallHost "nuc" "x86_64-linux");
      salsa = (mkHost "salsa" "x86_64-linux");
      salsa_install = (mkInstallHost "salsa" "x86_64-linux");
    };
    homeConfigurations = {
      fredrik = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages."x86_64-linux";
        modules = [ userSharedConfig (mkUser "fredrik") ];
      };
    };
  };
}
