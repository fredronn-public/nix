#!/usr/bin/env bash
HOST=$1
METHOD=$2
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
FLAKE="${SCRIPTPATH}"
nixos-rebuild --flake ${FLAKE}#$HOST --build-host localhost --target-host root@$HOST --impure $METHOD
