{ config, lib, pkgs, ... }: {
  imports = [
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64.nix>
    ./hosts/nixberry/nixos.nix
  ];
  # The installer starts with a "nixos" user to allow installation, so add the SSH key to
  # that user. Note that the key is, at the time of writing, put in `/etc/ssh/authorized_keys.d`
  # bzip2 compression takes loads of time with emulation, skip it.
  sdImage.compressImage = false;
}
