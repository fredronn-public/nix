{ lib, pkgs, config, sops-nix, ...}:{
    config = {
        #home.file.".ssh/yubi-rsa.pub".source = config.sops.secrets."yubi/ssh-rsa-pub".path;
        #home.file.".ssh/yuby-rsa-cert.pub".source = config.sops.secrets."yubi/ssh-rsa-cert".path;
        #home.file.".ssh/id_ed25519".source = config.sops.secrets."ssh_key/priv".path;
        #home.file.".ssh/id_ed25519.pub".source = config.sops.secrets."ssh_key/pub".path;
        #home.file.".ssh/id_ed25519-cert.pub".source = config.sops.secrets."ssh_key/cert".path;
        programs.git.userEmail = "fredronn@gmail.com";
        home.username = "fredrik"; home.homeDirectory = "/home/fredrik";
    };
}
