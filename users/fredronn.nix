let
    yubi = import ./yubico.nix;
in
{ lib, pkgs, ...}:{
    imports = [ 
        <home-manager/nixos>
    ];
    config = {
        home-manager.users.fredronn = (import ./defaults.nix);
    };
}
