{ lib, pkgs, ... }:
lib.hm.dag.entryAfter ["writeBoundary"] ''
    #!${pkgs.bash}/bin/bash
    rm -rf $HOME/.yubico && mkdir -p $HOME/.yubico
    cat <<EOF > $HOME/.yubico/challenge-6916501
    v2:f4005ecd3d93c995bd2d0bc3f1ce207a0edbfccb8350e59e2e1f2dc6f9130a9a3fe716f46ca006e022ad24a5fe29c43a99b40244579056704652c8948d24e1:9bf19df4354559aa3dc52e37fd0c77ba27b37301:12d39cbdf3c8173f982afd44010064070e92889650899ae730d27cb5cd5a8f5b:10000:2
    EOF
    chmod 400 $HOME/.yubico/*
''

