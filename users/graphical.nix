{ lib, pkgs, config, systemType, ... }:
let
    now_playing = pkgs.stdenv.mkDerivation {
        name = "myscript";
        buildInputs = [
            (pkgs.python3.withPackages (pythonPackages: with pythonPackages; [
                requests
            ]))
        ];
        unpackPhase = "true";
        installPhase = ''
        mkdir -p $out/bin
        cp ${./scripts/now_playing.py} $out/bin/now_playing
        chmod +x $out/bin/now_playing
        '';
    };

in
{
    home.packages = with pkgs; [
        cinnamon.nemo
        acpi
        alacritty
        arc-theme
        betterlockscreen
        blueman
        citrix_workspace
        dconf
        deluge-gtk
        dunst
        element-desktop
        enpass
        feh
        firefox
        flameshot
        fractal
        google-chrome
        jellyfin-media-player
        light
        logseq
        feh
        evince
        microsoft-edge
        mpv
        neofetch
        nitrogen
        now_playing
        pasystray
        pavucontrol
        picom-next
        psst
        sakura 
        signal-desktop
        tdesktop
        ulauncher
        (vscode-with-extensions.override {
            #vscode = vscodium;
            vscodeExtensions = with vscode-extensions; [
                jnoortheen.nix-ide
                gitlab.gitlab-workflow
                #ms-python.python
                ms-vscode-remote.remote-ssh
                ms-kubernetes-tools.vscode-kubernetes-tools
                redhat.vscode-yaml
                asvetliakov.vscode-neovim
            ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
                {
                    name = "remote-ssh-edit";
                    publisher = "ms-vscode-remote";
                    version = "0.47.2";
                    sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
                }
                {
                    name = "vscode-gitops-tools";
                    publisher = "Weaveworks";
                    version = "0.23.1";
                    sha256 = "YavJmCvUOZYNg6kqZFao4WIX1Rv1hVl5X40GsD1BPwk=";
                }
                {
                    name = "signageos-vscode-sops";
                    publisher = "signageos";
                    version = "0.7.1";
                    sha256 = "4jwxBMDN5a5p1JCNUmkhoCuqN8KZZRVfnM30yY0en1g=";
                }
            ];
        })
    ];
    gtk = {
        enable = true; 
        theme.name = "Arc-Dark"; 
        cursorTheme.name = "Breeze";
        gtk2.extraConfig = ''
            gtk-application-prefer-dark-theme = 1
        '';
        gtk3.extraConfig.gtk-application-prefer-dark-theme = 1;
        gtk4.extraConfig.gtk-application-prefer-dark-theme = 1;
    };
    qt = { enable = true; platformTheme = "gtk"; };
    home.pointerCursor = {
        package = pkgs.vanilla-dmz;
        name = "Vanilla-DMZ";
    };
    services.gpg-agent.pinentryFlavor = lib.mkForce "gtk2";
    home.file.".config/i3/config".text = (builtins.readFile ./i3.conf);
    home.file.".config/i3/i3blocks.conf".text = (builtins.readFile ./i3blocks.conf);
    home.file.".config/dunst/dunstrc".text = (builtins.readFile ./dunstrc);
    home.file.".config/alacritty/alacritty.yml".text = (builtins.readFile ./alacritty.yml);
    home.file.".config/polybar/config".text = (builtins.readFile ./polybar.config);
    home.file.".config/i3status-rust/config.toml".text = (builtins.readFile ./i3status-rs.toml);


    home.file.".config/HybridBar/config.json".text = ''
        {
            "hybrid": {
                "namespace": "hybrid-bar",
                "r": 10,
                "g": 10,
                "b": 10,
                "a": 1.0
            },
            "left-label_username": {
                "text": "user: ",
                "command": "whoami",
                "tooltip": "And here's your kernel: ",
                "tooltip_command": "uname -r",
                "update_rate": 50000
            },
            /*"centered-label_centerdummy": {
                "text": "Example Config - Create/Edit yours at ~/.config/HybridBar/config.json",
                "tooltip": "An example of a centered label"
            },*/
            "right-label_rightdummy": {
                "text": "t: ",
                "command": "date +%H:%M",
                "update_rate": 60
            }
        }
    '';
    home.file.".config/HybridBar/style.css".text = ''
        /* Label Color. Config default is white. */
        label {
            color:          white;
        }
        
        /* Button Configuration */
        button {
            font-weight:    normal;
            border:         none;
            box-shadow:     none;
            margin-bottom: -8px;
            margin-top:    -8px;
            padding-right:  0;
            padding-left:   0;
        }
    '';
    programs.rofi = { enable = true; theme = "${pkgs.rofi}/share/rofi/themes/android_notification.rasi"; };
}
