{
    config, lib, pkgs, sops-nix, ...
}:
with pkgs;
let
    oh-my-tmux = builtins.fetchGit { url = "https://github.com/gpakosz/.tmux.git"; };
    weechat = pkgs.weechat.override {
        configure = { availablePlugins, ... }: {
            scripts = with pkgs.weechatScripts; [
                wee-slack
                weechat-matrix
            ];
        };
    };

    obsidian-nvim = pkgs.vimUtils.buildVimPlugin {
        name = "vim-iced";
        src = pkgs.fetchFromGitHub {
            owner = "epwalsh";
            repo = "obsidian.nvim";
            rev = "main";
        };
        buildInputs = with pkgs; [ stylua lua53Packages.luacheck ];
    };

    kea_database_api = import (builtins.fetchGit {
        url = "https://gitlab.com/fredronn-public/kea-database-api.git";
        ref = "main";
        rev = "1b07af9fc3bf6bf566c7a9a03d6997bbced686ec";
    }) {};

in
{
    home.packages = with pkgs; [
        direnv fzf btop rnix-lsp nixopsUnstable nnn bat
        kubectl kustomize kubeconform yamllint krew
        kubernetes-helm age sops jq yq-go
        nmap mtr fping wget curl
        glab git-crypt
        home-assistant-cli
        weechat mosquitto
        bfg-repo-cleaner tuir
        fluxcd k9s 
        terraform nixos-generators velero ookla-speedtest
        greg w3m
#        kea_database_api
    ];
    programs.git = {
        enable = true;
        userName = "Fredrik Rönnvall";
        signing = { key = "44397BDDDF023451"; signByDefault = false; };
        extraConfig = {
            init = { defaultBranch = "main"; };
            diff = {
                sopsdiffer.textconv = "sops -d --config /dev/null";
            };
        };
    };
    home.activation.yubi = lib.hm.dag.entryAfter ["writeBoundary"] ''
        #!${pkgs.bash}/bin/bash
        rm -rf $HOME/.yubico && mkdir -p $HOME/.yubico
        cat <<EOF > $HOME/.yubico/challenge-6916501
        v2:f4005ecd3d93c995bd2d0bc3f1ce207a0edbfccb8350e59e2e1f2dc6f9130a9a3fe716f46ca006e022ad24a5fe29c43a99b40244579056704652c8948d24e1:9bf19df4354559aa3dc52e37fd0c77ba27b37301:12d39cbdf3c8173f982afd44010064070e92889650899ae730d27cb5cd5a8f5b:10000:2
        EOF
        chmod 400 $HOME/.yubico/*
    '';
    services.gpg-agent = {
        enable = true;
        enableSshSupport = true;
        enableFishIntegration = true;
        pinentryFlavor = "curses";
        defaultCacheTtl = 43200;
        defaultCacheTtlSsh = 43200;
        maxCacheTtl = 43200;
        maxCacheTtlSsh = 43200;
    };
    programs.gpg = {
        enable = true;
        publicKeys = [
            { source = ./yubi.gpg.pub; trust = "ultimate"; }
        ];
    };

    programs.ssh = {
        enable = true;
        includes = [ "conf.d/*.conf" ];
        controlMaster = "auto";
        compression = true;
        controlPath = "~/.ssh/control-%h-%p-%r";
        controlPersist = "yes";
        forwardAgent = true;
    };

    programs.neovim = {
        enable = true;
        plugins = with pkgs.vimPlugins; [
            neogit
            deoplete-nvim
            fzf-vim
            indentLine
            nerdtree
            vim-airline
            vim-gitgutter
            vim-gnupg
            wombat256-vim
            limelight-vim
            goyo-vim
            nvim-lint
        ];
        extraConfig = ''
            set background=dark
            colorscheme wombat256mod
            set backspace=start,indent,eol
            set number
            set autoindent
            set smartindent
            set tabstop=4
            set shiftwidth=4
            set expandtab
            syntax on
            filetype plugin indent on
            set laststatus=2
            set colorcolumn=+1
            set pastetoggle=<F2>
            let mapleader=","
            nnoremap <leader>T :NERDTreeToggle<CR>
            nnoremap <leader>t :Files<CR>
            nnoremap <leader>g :GFiles<CR>
            nnoremap <leader>b :Buffers<CR>
        '';
        extraLuaConfig = ''
        
          require('lint').linters_by_ft = {
            yaml = {'yamllint',}
          }

          vim.api.nvim_create_autocmd({ "BufWritePost" }, {
            callback = function()
              require("lint").try_lint()
            end,
          })

        '';

    };

    accounts.email.accounts.gmail = {
        address = "fredronn@gmail.com";
        aliases = [
            "fredrik@ronnvall.com"
        ];
        userName = "fredronn@gmail.com";
        mbsync = {
            enable = true;
            create = "maildir";
        };
        msmtp.enable = true;
        neomutt = {
            enable = true;
            extraConfig = ''
            auto_view text/plain text/utf-8 text/html
            alternative_order text/html text/plain
            '';
        };
        notmuch.enable = true;
        notmuch.neomutt = { 
            enable = true;
            virtualMailboxes = [
                { name = "INBOX"; query = "tag:inbox"; }
            ];
        };
        primary = true;
        realName = "Fredrik Rönnvall";
        imap.host = "imap.gmail.com";
        smtp.host = "smtp.gmail.com";
        passwordCommand = "pass show Email/fredronn@gmail.com";
        signature = {
            text = ''
            Kind Regards
            Fredrik Rönnvall
            '';
            showSignature = "append";
        };
    };

    programs.neomutt = {
        enable = true;
        editor = "nvim";
        sort = "reverse-threads";
        checkStatsInterval = 300;
        settings = {
            show_multipart_alternative = "inline";
        };
    };

    programs.password-store.enable = true;
    programs.mbsync.enable = true;
    programs.msmtp.enable = true;
    programs.notmuch = {
        enable = true;
        hooks = {
            preNew = "mbsync --all";
            postNew = ''
                notmuch tag +inbox -- tag:new
                notmuch tag +ronnvall -- tag:new and to:ronnvall.com
                notmuch tag +sent -inbox -- tag:new and from:fredrik@ronnvall.com
                notmuch tag +sent -inbox -- tag:new and from:fredronn@gmail.com
                notmuch tag +chalmers -- tag:new and from:chalmers.se
                notmuch tag +chalmers -- tag:new to:chalmers.se
                notmuch tag -new -- tag:new
            '';
        };
        new.tags = [ "new" ];
    };

    programs.bash.enable = true;

    programs.fish = {
        enable = true;
        interactiveShellInit = ''
            # Read sensitive env vars
            export (cat $XDG_RUNTIME_DIR/secrets/environment | xargs -L1)
            if set -q SSH_TTY
                if not set -q TMUX 
                    set -g TMUX tmux new-session -d -s base
                    eval $TMUX
                    tmux attach-session -d -t base
                end
            end
            alias ndu 'nnn -T d'
        '';
        plugins = [
            { name = "coffeeandcode"; src = pkgs.fetchFromGitHub { owner = "oh-my-fish"; repo = "theme-coffeeandcode"; rev = "master"; sha256 = "D8rZufX55JEgLcjse7uzNwj3/QUQnIHnrfI+eqx+iUg="; } ;}
            { name = "direnv"; src = pkgs.fetchFromGitHub { owner = "oh-my-fish"; repo = "plugin-direnv"; rev = "master"; sha256 = "50tMKwtXtJBpgZ42JfJKyIWgusu4xZ9/yCiGKDfqyhE="; } ;}
            { name = "fzf"; src = pkgs.fetchFromGitHub { owner = "jethrokuan"; repo = "fzf"; rev = "master"; sha256 = "28QW/WTLckR4lEfHv6dSotwkAKpNJFCShxmKFGQQ1Ew="; } ;}
        ];
    };

    # Sync oh-my-tmux
    home.file.".tmux".source = oh-my-tmux;
    
    # Link the generic conf
    home.file.".tmux.conf".source = "${oh-my-tmux}/.tmux.conf";
    home.file.".tmux.conf.local".text = (builtins.readFile ./.tmux.conf.local);

    home.file.".config/greg/greg.conf".text = (builtins.readFile ./greg.conf);

    home.file.".config/iamb/config.json".text = ''
    {
        "profiles": {
            "ronnvall.com": {
                "url": "https://matrix.ronnvall.com",
                "user_id": "@fredrik:ronnvall.com"
            }
        }
    }
    '';
    home.file.".mailcap".text = ''
    text/html; w3m -I %{charset} -T text/html; copiousoutput;
    application/pdf; evince %s
    image/*; feh --scale-down -g 640x480 %s
    '';

    home.file.".config/yamllint/config".text = (builtins.readFile ./yamllint.yaml);

    # Environment
    home.sessionVariables = {
        EDITOR = "nvim";
    };

    sops.secrets."yubi/ssh-rsa-pub" = { sopsFile = ./yubi.yaml; path = ".ssh/ssh-rsa.pub";};
    sops.secrets."yubi/ssh-rsa-cert" = { sopsFile = ./yubi.yaml; path = ".ssh/ssh-rsa-cert.pub"; };
    sops.secrets."ssh_key/pub" = { sopsFile = ./ssh_key.yaml; path = ".ssh/id_ed25519.pub"; };
    sops.secrets."ssh_key/cert" = { sopsFile = ./ssh_key.yaml; path = ".ssh/id_ed25519-cert.pub"; };
    sops.secrets."ssh_key/priv" = { sopsFile = ./ssh_key.yaml; path = ".ssh/id_ed25519"; };
    sops.secrets.environment = { sopsFile = ./environment.yaml; };
}
