# NIX Configs

This repo contains my nix configurations for my systems. Its not well documented, its messy, some of it is not in a finished state.

# Build NIX ISO

Use nixos-generate

```bash
nixos-generate --flake .#iso -f iso
```

# Build Raspberry SD Image

```bash
nixos-generate -f sd-aarch64 --flake .#nixberry
```
