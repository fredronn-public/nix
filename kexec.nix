{ config, pkgs, lib, ... }:{
    imports = [
        <nixpkgs/nixos/modules/profiles/headless.nix>
        <nixpkgs/nixos/modules/profiles/minimal.nix>
        <nixpkgs/nixos/modules/profiles/all-hardware.nix>
        <nixpkgs/nixos/modules/installer/netboot/netboot.nix>
        <nixpkgs/nixos/modules/installer/cd-dvd/channel.nix>
  ];

    config = {
        system.stateVersion = "23.05";
        nix.settings.experimental-features = [ "nix-command" "flakes" ];
        environment.systemPackages = [];
        environment.defaultPackages = [];
        services.openssh.enable = true;
        users.users.root.openssh.authorizedKeys.keyFiles = [ ./../../.ssh/id_ed25519.pub ];
        security.sudo.enable = false;
        services.udisks2.enable = false;
        networking.firewall.enable = false;
    };
}
